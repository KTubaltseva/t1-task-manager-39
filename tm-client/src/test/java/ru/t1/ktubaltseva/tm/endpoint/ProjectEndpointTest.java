package ru.t1.ktubaltseva.tm.endpoint;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ktubaltseva.tm.api.endpoint.IAuthEndpoint;
import ru.t1.ktubaltseva.tm.api.endpoint.IProjectEndpoint;
import ru.t1.ktubaltseva.tm.api.endpoint.IUserEndpoint;
import ru.t1.ktubaltseva.tm.api.service.ILoggerService;
import ru.t1.ktubaltseva.tm.api.service.IPropertyService;
import ru.t1.ktubaltseva.tm.dto.request.project.*;
import ru.t1.ktubaltseva.tm.dto.request.user.UserLoginRequest;
import ru.t1.ktubaltseva.tm.dto.request.user.UserLogoutRequest;
import ru.t1.ktubaltseva.tm.dto.request.user.UserRegistryRequest;
import ru.t1.ktubaltseva.tm.dto.request.user.UserRemoveRequest;
import ru.t1.ktubaltseva.tm.dto.response.project.*;
import ru.t1.ktubaltseva.tm.dto.response.user.UserLoginResponse;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.marker.IntegrationCategory;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.service.LoggerService;
import ru.t1.ktubaltseva.tm.service.PropertyService;

import java.security.NoSuchAlgorithmException;

import static ru.t1.ktubaltseva.tm.constant.ProjectTestData.*;

@Category(IntegrationCategory.class)
public final class ProjectEndpointTest {

    @NotNull
    private final static ILoggerService loggerService = new LoggerService();

    @NotNull
    private final static IPropertyService propertyService = new PropertyService(loggerService);

    @NotNull
    private final static IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private final static IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService);

    @NotNull
    private final static IProjectEndpoint endpoint = IProjectEndpoint.newInstance(propertyService);

    @Nullable
    private static String adminToken;

    @Nullable
    private static String userToken;

    @Nullable
    private static Project userProject;

    @Nullable
    private static String userProjectId;

    @BeforeClass
    public static void before() throws AbstractException, NoSuchAlgorithmException, JsonProcessingException {
        @NotNull final UserLoginRequest loginAdminRequest = new UserLoginRequest(ADMIN_LOGIN, ADMIN_PASSWORD);
        @NotNull final UserLoginResponse loginAdminResponse = authEndpoint.login(loginAdminRequest);
        adminToken = loginAdminResponse.getToken();

        @NotNull final UserRegistryRequest userRegistryRequest = new UserRegistryRequest(USER_LOGIN, USER_PASSWORD, USER_EMAIL);
        userEndpoint.registry(userRegistryRequest);

        @NotNull final UserLoginRequest loginUserRequest = new UserLoginRequest(USER_LOGIN, USER_PASSWORD);
        @NotNull final UserLoginResponse loginUserResponse = authEndpoint.login(loginUserRequest);
        userToken = loginUserResponse.getToken();

        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(userToken, PROJECT_NAME, PROJECT_DESC);
        @NotNull final ProjectCreateResponse projectCreateResponse = endpoint.createProject(projectCreateRequest);
        userProject = projectCreateResponse.getProject();
        userProjectId = userProject.getId();
    }

    @AfterClass
    public static void after() throws AbstractException {
        endpoint.clearProjects(new ProjectClearRequest(userToken));
        userProject = null;
        userProjectId = null;

        @NotNull final UserLogoutRequest logoutUserRequest = new UserLogoutRequest(userToken);
        authEndpoint.logout(logoutUserRequest);
        userToken = null;

        @NotNull final UserRemoveRequest removeUserRequest = new UserRemoveRequest(adminToken, USER_LOGIN);
        userEndpoint.remove(removeUserRequest);

        @NotNull final UserLogoutRequest logoutAdminRequest = new UserLogoutRequest(adminToken);
        authEndpoint.logout(logoutAdminRequest);
        adminToken = null;
    }

    @Test
    public void getProjectById() throws AbstractException {
        @Nullable final ProjectDisplayByIdResponse response = endpoint.getProjectById(new ProjectDisplayByIdRequest(userToken, userProjectId));
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getProject());
    }

    @Test
    public void getAllProjects() throws AbstractException {
        @NotNull final ProjectDisplayListResponse responseNoSort = endpoint.getAllProjects(new ProjectDisplayListRequest(userToken));
        Assert.assertNotNull(responseNoSort);
        Assert.assertNotNull(responseNoSort.getProjects());

        @NotNull final ProjectDisplayListResponse responseWithSort = endpoint.getAllProjects(new ProjectDisplayListRequest(userToken, PROJECT_SORT));
        Assert.assertNotNull(responseWithSort);
        Assert.assertNotNull(responseWithSort.getProjects());
    }

    @Test
    public void clearProjects() throws AbstractException {
        @NotNull final ProjectClearResponse response = endpoint.clearProjects(new ProjectClearRequest(userToken));
        Assert.assertNotNull(response);

        @NotNull final ProjectDisplayListResponse responseGetAll = endpoint.getAllProjects(new ProjectDisplayListRequest(userToken, PROJECT_SORT));
        Assert.assertNull(responseGetAll.getProjects());

        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(userToken, PROJECT_NAME, PROJECT_DESC);
        @NotNull final ProjectCreateResponse projectCreateResponse = endpoint.createProject(projectCreateRequest);
        userProject = projectCreateResponse.getProject();
        userProjectId = userProject.getId();
    }

    @Test
    public void removeProjectById() throws AbstractException {
        @Nullable final ProjectRemoveByIdResponse response = endpoint.removeProjectById(new ProjectRemoveByIdRequest(userToken, userProjectId));
        Assert.assertNotNull(response);

        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(userToken, PROJECT_NAME, PROJECT_DESC);
        @NotNull final ProjectCreateResponse projectCreateResponse = endpoint.createProject(projectCreateRequest);
        userProject = projectCreateResponse.getProject();
        userProjectId = userProject.getId();
    }

    @Test
    public void createProject() throws AbstractException {
        @NotNull final ProjectCreateResponse response = endpoint.createProject(new ProjectCreateRequest(userToken, PROJECT_NAME, PROJECT_DESC));
        Assert.assertNotNull(response);
        @Nullable final Project project = response.getProject();
        Assert.assertNotNull(project);
        Assert.assertNotNull(endpoint.getProjectById(new ProjectDisplayByIdRequest(userToken, project.getId())));
    }

    @Test
    public void changeProjectStatusById() throws AbstractException {
        @NotNull final ProjectChangeStatusByIdResponse response = endpoint.changeProjectStatusById(new ProjectChangeStatusByIdRequest(userToken, userProjectId, PROJECT_STATUS));
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getProject().getStatus());
        Assert.assertEquals(PROJECT_STATUS, response.getProject().getStatus());
    }

    @Test
    public void updateProjectById() throws AbstractException {
        @NotNull final ProjectUpdateByIdResponse response = endpoint.updateProjectById(new ProjectUpdateByIdRequest(userToken, userProjectId, PROJECT_NAME_NEW, PROJECT_DESC_NEW));
        Assert.assertNotNull(response);
        Assert.assertEquals(PROJECT_NAME_NEW, response.getProject().getName());
        Assert.assertEquals(PROJECT_DESC_NEW, response.getProject().getDescription());
    }

    @Test
    public void startProjectById() throws AbstractException {
        @NotNull final ProjectStartByIdResponse response = endpoint.startProjectById(new ProjectStartByIdRequest(userToken, userProjectId));
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getProject().getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, response.getProject().getStatus());
    }

    @Test
    public void completeProjectById() throws AbstractException {
        @NotNull final ProjectCompleteByIdResponse response = endpoint.completeProjectById(new ProjectCompleteByIdRequest(userToken, userProjectId));
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getProject().getStatus());
        Assert.assertEquals(Status.COMPLETED, response.getProject().getStatus());
    }
}

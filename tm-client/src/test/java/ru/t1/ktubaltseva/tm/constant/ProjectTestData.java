package ru.t1.ktubaltseva.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.enumerated.Sort;
import ru.t1.ktubaltseva.tm.enumerated.Status;

@UtilityClass
public final class ProjectTestData {

    @Nullable
    public final static String ADMIN_LOGIN = "ADMIN";

    @Nullable
    public final static String ADMIN_PASSWORD = "ADMIN";

    @Nullable
    public final static String USER_LOGIN = "TEST";

    @Nullable
    public final static String USER_LOGIN_NULL = null;

    @Nullable
    public final static String USER_LOGIN_NON_EXISTENT = "USER_LOGIN_NON_EXISTENT";

    @Nullable
    public final static String USER_PASSWORD = "TEST";

    @Nullable
    public final static String USER_PASSWORD_NULL = null;

    @Nullable
    public final static String USER_REGISTRY_LOGIN = "REGISTRY";

    @Nullable
    public final static String USER_REGISTRY_PASSWORD = "REGISTRY";

    @Nullable
    public final static String USER_REGISTRY_EMAIL = "REGISTRY";

    @Nullable
    public final static String USER_EMAIL = "USER_EMAIL";

    @Nullable
    public final static String USER_EMAIL_NULL = null;

    @Nullable
    public final static String USER_TOKEN_NULL = null;

    @Nullable
    public final static String USER_TOKEN_NON_EXISTENT = "USER_TOKEN_NON_EXISTENT";

    @Nullable
    public final static String USER_FIRST_NAME = "USER_FIRST_NAME";

    @Nullable
    public final static String NULL_FIRST_NAME = null;

    @Nullable
    public final static String USER_MIDDLE_NAME = "USER_MIDDLE_NAME";

    @Nullable
    public final static String NULL_MIDDLE_NAME = null;

    @Nullable
    public final static String USER_LAST_NAME = "USER_LAST_NAME";

    @Nullable
    public final static String NULL_LAST_NAME = null;

    @Nullable
    public final static String NULL_PROJECT_ID = null;

    @Nullable
    public final static String NON_EXISTENT_PROJECT_ID = "NON_EXISTENT_PROJECT_ID";

    @Nullable
    public final static String NULL_NAME = null;

    @Nullable
    public final static String NULL_DESC = null;

    @Nullable
    public final static String PROJECT_NAME = "PROJECT_NAME";

    @Nullable
    public final static String PROJECT_DESC = "PROJECT_DESC";

    @Nullable
    public final static String PROJECT_NAME_NEW = "PROJECT_NAME_NEW";

    @Nullable
    public final static String PROJECT_DESC_NEW = "PROJECT_DESC_NEW";

    @Nullable
    public final static Status NULL_STATUS = null;

    @Nullable
    public final static Status PROJECT_STATUS = Status.IN_PROGRESS;

    @Nullable
    public final static Sort NULL_SORT = null;

    @Nullable
    public final static Sort PROJECT_SORT = Sort.BY_CREATED;

}

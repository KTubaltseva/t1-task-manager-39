package ru.t1.ktubaltseva.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.command.AbstractCommand;
import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.TaskNotFoundException;
import ru.t1.ktubaltseva.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void renderTasks(@Nullable final List<Task> tasks) throws TaskNotFoundException {
        if (tasks == null) throw new TaskNotFoundException();
        int index = 1;
        for (@NotNull final Task task : tasks) {
            System.out.println(index + ". " + task);
            System.out.println();
            index++;
        }
    }

    public void displayTask(@Nullable final Task task) throws TaskNotFoundException {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESC: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
        System.out.println("USER ID: " + task.getUserId());
    }

    protected void renderTasksFullInfo(@Nullable final List<Task> tasks) throws TaskNotFoundException {
        if (tasks == null) throw new TaskNotFoundException();
        int index = 1;
        for (@NotNull final Task task : tasks) {
            System.out.println(index + ".");
            displayTask(task);
            System.out.println();
            index++;
        }
    }

}
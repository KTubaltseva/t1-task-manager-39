package ru.t1.ktubaltseva.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.dto.request.data.DataLoadXmlFasterXmlRequest;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

public class DataLoadXmlFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    private final String NAME = "data-load-xml-faster-xml";

    @NotNull
    private final String DESC = "Load data from xml file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[LOAD XML DATA]");
        @NotNull final DataLoadXmlFasterXmlRequest request = new DataLoadXmlFasterXmlRequest(getToken());
        getDomainEndpoint().loadDataXmlFasterXml(request);
    }

}

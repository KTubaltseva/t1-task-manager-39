package ru.t1.ktubaltseva.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.command.AbstractCommand;

import java.util.Collection;

public final class ApplicationHelpCommand extends AbstractSystemCommand {

    @NotNull
    private final String NAME = "help";

    @NotNull
    private final String DESC = "Display list of commands.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        @NotNull String ARGUMENT = "-h";
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (@NotNull final AbstractCommand command : commands) System.out.println(command);
    }

}

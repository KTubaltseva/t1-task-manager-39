package ru.t1.ktubaltseva.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.dto.request.task.TaskBindToProjectRequest;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.util.TerminalUtil;

public final class TaskBindToProjectCommand extends AbstractTaskCommand {

    @NotNull
    private final String NAME = "task-bind-to-project";

    @NotNull
    private final String DESC = "Bind task to project.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(getToken(), taskId, projectId);
        getTaskEndpoint().bindTaskToProject(request);
    }

}

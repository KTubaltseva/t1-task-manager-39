package ru.t1.ktubaltseva.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.dto.request.project.ProjectCreateRequest;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @NotNull
    private final String NAME = "project-create";

    @NotNull
    private final String DESC = "Create new project.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(getToken(), name, description);
        getProjectEndpoint().createProject(request);
    }

}

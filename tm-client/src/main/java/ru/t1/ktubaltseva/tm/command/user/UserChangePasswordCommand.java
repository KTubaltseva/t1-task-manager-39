package ru.t1.ktubaltseva.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.dto.request.user.UserChangePasswordRequest;
import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.util.TerminalUtil;

import java.security.NoSuchAlgorithmException;

public class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "user-change-password";

    @NotNull
    private final String DESC = "Change user password.";

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException, NoSuchAlgorithmException {
        System.out.println("[CHANGE USER PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(getToken(), password);
        getUserEndpoint().changePassword(request);
    }

}

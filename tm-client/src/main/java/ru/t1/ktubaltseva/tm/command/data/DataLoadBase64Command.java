package ru.t1.ktubaltseva.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.dto.request.data.DataLoadBase64Request;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

public class DataLoadBase64Command extends AbstractDataCommand {

    @NotNull
    private final String NAME = "data-load-base64";

    @NotNull
    private final String DESC = "Load data from base64 file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[LOAD BASE64 DATA]");
        @NotNull final DataLoadBase64Request request = new DataLoadBase64Request(getToken());
        getDomainEndpoint().loadDataBase64(request);
    }

}

package ru.t1.ktubaltseva.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.repository.IProjectRepository;
import ru.t1.ktubaltseva.tm.api.service.IConnectionService;
import ru.t1.ktubaltseva.tm.api.service.IProjectService;
import ru.t1.ktubaltseva.tm.comparator.CreatedComparator;
import ru.t1.ktubaltseva.tm.comparator.NameComparator;
import ru.t1.ktubaltseva.tm.comparator.StatusComparator;
import ru.t1.ktubaltseva.tm.enumerated.Sort;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.ProjectNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.*;
import ru.t1.ktubaltseva.tm.model.Project;

import java.util.*;

public final class ProjectService implements IProjectService {

    @NotNull
    private final IConnectionService connectionService;

    public ProjectService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    public Project add(@Nullable final Project project) throws AbstractException {
        if (project == null) throw new ProjectNotFoundException();
        @Nullable Project resultProject;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.add(project);
            sqlSession.commit();
            resultProject = findOneById(project.getId());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return resultProject;
    }

    @NotNull
    @Override
    public Project add(
            @Nullable final String userId,
            @Nullable final Project project
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (project == null) throw new ProjectNotFoundException();
        project.setUserId(userId);
        @Nullable Project resultProject;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.add(project);
            sqlSession.commit();
            resultProject = findOneById(project.getId());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return resultProject;
    }

    @NotNull
    @Override
    public Collection<Project> add(@Nullable final Collection<Project> projects) throws AbstractException {
        if (projects == null || projects.isEmpty()) return Collections.emptyList();
        @NotNull List<Project> resultProjects = new ArrayList<>();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            for (@NotNull final Project project : projects) {
                projectRepository.add(project);
            }
            sqlSession.commit();
            for (@NotNull final Project project : projects) {
                resultProjects.add(findOneById(project.getId()));
            }
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return resultProjects;
    }

    @Override
    public void clear() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.clear();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void clear(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.clearByUserId(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) throws AbstractException {
        try {
            findOneById(id);
        } catch (ProjectNotFoundException e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        try {
            findOneById(userId, id);
        } catch (ProjectNotFoundException e) {
            return false;
        }
        return true;
    }

    @NotNull
    @Override
    public List<Project> findAll() throws AbstractException {
        @Nullable List<Project> resultProjects;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            resultProjects = projectRepository.findAll();
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            sqlSession.close();
        }
        return resultProjects;
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        @Nullable List<Project> resultProjects;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            resultProjects = projectRepository.findAllByUserId(userId);
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            sqlSession.close();
        }
        return resultProjects;
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final Comparator<Project> comparator) throws AbstractException {
        if (comparator == null) return findAll();
        @Nullable List<Project> resultProjects;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            resultProjects = projectRepository.findAllSort(getSortColumnName(comparator));
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            sqlSession.close();
        }
        return resultProjects;
    }

    @NotNull
    @Override
    public List<Project> findAll(
            @Nullable final String userId,
            @Nullable final Comparator<Project> comparator
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (comparator == null) return findAll(userId);
        @Nullable List<Project> resultProjects;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            resultProjects = projectRepository.findAllSortByUserId(userId, getSortColumnName(comparator));
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            sqlSession.close();
        }
        return resultProjects;
    }

    @NotNull
    @Override
    public List<Project> findAll(
            @Nullable final String userId,
            @Nullable final Sort sort
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (sort == null) return findAll(userId);
        @Nullable List<Project> resultProjects;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            resultProjects = projectRepository.findAllSortByUserId(userId, getSortColumnName(sort.getComparator()));
        } finally {
            sqlSession.close();
        }
        return resultProjects;
    }

    @NotNull
    @Override
    public Project findOneById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Project resultProject;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            resultProject = projectRepository.findOneById(id);
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            sqlSession.close();
        }
        if (resultProject == null) throw new ProjectNotFoundException();
        return resultProject;
    }

    @NotNull
    @Override
    public Project findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Project resultProject;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            resultProject = projectRepository.findOneByIdByUserId(userId, id);
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            sqlSession.close();
        }
        if (resultProject == null) throw new ProjectNotFoundException();
        return resultProject;
    }

    @Override
    public int getSize() throws AbstractException {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        int result = 0;
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            result = projectRepository.getSize();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @NotNull
    private String getSortColumnName(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        if (comparator == NameComparator.INSTANCE) return "name";
        if (comparator == StatusComparator.INSTANCE) return "status";
        return "created";
    }

    @Override
    public void removeOne(@Nullable final Project project) throws AbstractException {
        if (project == null) throw new ProjectNotFoundException();
        removeById(project.getId());
    }

    @NotNull
    @Override
    public void removeOne(
            @Nullable final String userId,
            @Nullable final Project project
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (project == null) throw new ProjectNotFoundException();
        removeById(userId, project.getId());
    }

    @NotNull
    @Override
    public void removeById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (!existsById(id)) throw new ProjectNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.removeById(id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public void removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (!existsById(userId, id)) throw new ProjectNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.removeByIdByUserId(userId, id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public Collection<Project> set(@Nullable final Collection<Project> projects) throws AbstractException {
        clear();
        if (projects == null || projects.isEmpty()) return Collections.emptyList();
        return add(projects);
    }

    @NotNull
    public Project update(
            @Nullable final String userId,
            @Nullable final Project project
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (!existsById(userId, project.getId())) throw new ProjectNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @Nullable Project resultProject;
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.update(project);
            sqlSession.commit();
            resultProject = findOneById(project.getId());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return resultProject;
    }

    @NotNull
    @Override
    public Project changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable Project resultProject = findOneById(userId, id);
        resultProject.setStatus(status);
        resultProject = update(userId, resultProject);
        return resultProject;
    }

    @NotNull
    @Override
    public Project create(
            @Nullable final String userId,
            @Nullable final String name
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull Project project = new Project(name);
        project.setUserId(userId);
        return add(project);
    }

    @NotNull
    @Override
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull Project project = new Project(name, description);
        project.setUserId(userId);
        return add(project);
    }

    @NotNull
    @Override
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable Project resultProject = findOneById(userId, id);
        resultProject.setName(name);
        resultProject.setDescription(description);
        resultProject = update(userId, resultProject);
        return resultProject;
    }

}

package ru.t1.ktubaltseva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.model.User;

import java.security.NoSuchAlgorithmException;

public interface IUserService extends IService<User> {

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password
    ) throws NoSuchAlgorithmException, AbstractException;

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    ) throws AbstractException, NoSuchAlgorithmException;

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password,
            @Nullable Role role
    ) throws AbstractException, NoSuchAlgorithmException;

    @NotNull
    User findByLogin(@Nullable String login) throws AbstractException;

    @NotNull
    User findByEmail(@Nullable String email) throws AbstractException;

    @NotNull
    Boolean isLoginExists(@Nullable String login) throws AbstractException;

    @NotNull
    Boolean isEmailExists(@Nullable String email) throws AbstractException;

    @NotNull
    User lockUserByLogin(@Nullable String login) throws AbstractException;

    void removeByLogin(@Nullable String login) throws AbstractException;

    void removeByEmail(@Nullable String email) throws AbstractException;

    @NotNull
    User setPassword(
            @Nullable String id,
            @Nullable String password
    ) throws NoSuchAlgorithmException, AbstractException;

    @NotNull
    User unlockUserByLogin(@Nullable String login) throws AbstractException;

    @NotNull User update(
            @Nullable User user
    ) throws AbstractException;

    @NotNull
    User updateById(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String middleName,
            @Nullable String lastName
    ) throws AbstractException;
}

package ru.t1.ktubaltseva.tm.api.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.model.Session;
import ru.t1.ktubaltseva.tm.model.User;

import java.security.NoSuchAlgorithmException;

public interface IAuthService {

    @NotNull
    String login(
            @Nullable String login,
            @Nullable String password
    ) throws AbstractException, NoSuchAlgorithmException, JsonProcessingException;

    @NotNull
    User registry(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    ) throws AbstractException, NoSuchAlgorithmException;

    @NotNull
    Session validateToken(
            @Nullable final String token
    );

    void invalidate(
            @Nullable final Session session
    ) throws EntityNotFoundException, IdEmptyException, AbstractException;

}

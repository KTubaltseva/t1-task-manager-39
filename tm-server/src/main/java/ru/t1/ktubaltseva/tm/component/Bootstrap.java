package ru.t1.ktubaltseva.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.api.endpoint.*;
import ru.t1.ktubaltseva.tm.api.service.*;
import ru.t1.ktubaltseva.tm.endpoint.*;
import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.Task;
import ru.t1.ktubaltseva.tm.model.User;
import ru.t1.ktubaltseva.tm.service.*;
import ru.t1.ktubaltseva.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.ktubaltseva.tm.command";

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService(loggerService);

    @Getter
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(connectionService);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(
            propertyService,
            connectionService
    );

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService, sessionService);

    @Getter
    @NotNull
    private final Backup backup = new Backup(this);

    @Getter
    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @Getter
    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @Getter
    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @Getter
    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @Getter
    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @Getter
    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    {
        registry(authEndpoint);
        registry(domainEndpoint);
        registry(projectEndpoint);
        registry(systemEndpoint);
        registry(taskEndpoint);
        registry(userEndpoint);
    }

    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        try {
            Files.write(Paths.get(filename), pid.getBytes());
            @NotNull final File file = new File(filename);
            file.deleteOnExit();
        } catch (IOException e) {
            loggerService.error(e);
        }
    }

    private void initDemoData() throws AbstractException, NoSuchAlgorithmException {
        if (userService.getSize() == 0) {
            @NotNull final User user1 = userService.create("USER1", "USER1");
            @NotNull final User user2 = userService.create("USER2", "USER2");
            @NotNull final User admin = userService.create("ADMIN", "ADMIN", Role.ADMIN);

            projectService.add(user1.getId(), new Project("pn2", "pd2", Status.IN_PROGRESS));
            projectService.add(user1.getId(), new Project("pn5", "pd5", Status.NOT_STARTED));
            projectService.add(user2.getId(), new Project("pn3", "pd3", Status.IN_PROGRESS));
            projectService.add(admin.getId(), new Project("pn4", "pd4", Status.COMPLETED));

            taskService.add(user1.getId(), new Task("tn2", "td2", Status.IN_PROGRESS));
            taskService.add(user1.getId(), new Task("tn5", "td5", Status.NOT_STARTED));
            taskService.add(user2.getId(), new Task("tn3", "td3", Status.IN_PROGRESS));
            taskService.add(admin.getId(), new Task("tn4", "td4", Status.COMPLETED));
        }
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = getPropertyService().getServerHost();
        @NotNull final Integer port = getPropertyService().getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    public void start() throws AbstractException, NoSuchAlgorithmException, SQLException {
        initPID();
        initDemoData();
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
    }

    @SneakyThrows
    private void stop() {
        loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
    }

}

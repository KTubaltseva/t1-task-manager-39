package ru.t1.ktubaltseva.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.model.User;

import java.util.List;

public interface IUserRepository {

    @Insert("INSERT INTO tm_user (id, created, login, passwordhash, email, firstname, middlename, lastname, role, locked) " +
            "VALUES (#{id}, #{created}, #{login}, #{passwordHash}, #{email}, #{firstName}, #{middleName}, #{lastName}, #{role}, #{locked})")
    @NotNull
    void add(@NotNull User user);

    @Delete("DELETE FROM tm_user")
    void clear();

    @NotNull
    @Select("SELECT * FROM tm_user")
    @Results(value = {
            @Result(property = "passwordHash", column = "passwordhash"),
            @Result(property = "firstName", column = "firstname"),
            @Result(property = "middleName", column = "middlename"),
            @Result(property = "lastName", column = "lastname")
    })
    List<User> findAll();

    @NotNull
    @Select("SELECT * FROM tm_user ORDER BY #{order}")
    @Results(value = {
            @Result(property = "passwordHash", column = "passwordhash"),
            @Result(property = "firstName", column = "firstname"),
            @Result(property = "middleName", column = "middlename"),
            @Result(property = "lastName", column = "lastname")
    })
    List<User> findAllSort(@NotNull @Param("order") String order);

    @Nullable
    @Select("SELECT * FROM tm_user WHERE id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "passwordhash"),
            @Result(property = "firstName", column = "firstname"),
            @Result(property = "middleName", column = "middlename"),
            @Result(property = "lastName", column = "lastname")
    })
    User findOneById(@NotNull @Param("id") String id);

    @Select("SELECT COUNT(*) FROM tm_user")
    int getSize();

    @Nullable
    @Delete("DELETE FROM tm_user WHERE id = #{id}")
    void removeById(@NotNull @Param("id") String id);

    @NotNull
    @Update("UPDATE tm_user " +
            "SET " +
            "login = #{login}, " +
            "passwordhash = #{passwordHash}, " +
            "firstname= #{firstName}, " +
            "middlename= #{middleName}, " +
            "lastname= #{lastName}, " +
            "role= #{role}, " +
            "locked= #{locked} " +
            "WHERE id = #{id}")
    void update(@NotNull User user);

    @Nullable
    @Select("SELECT * FROM tm_user WHERE login = #{login} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "passwordhash"),
            @Result(property = "firstName", column = "firstname"),
            @Result(property = "middleName", column = "middlename"),
            @Result(property = "lastName", column = "lastname")
    })
    User findByLogin(@NotNull @Param("login") String login);

    @Nullable
    @Select("SELECT * FROM tm_user WHERE email = #{email} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "passwordhash"),
            @Result(property = "firstName", column = "firstname"),
            @Result(property = "middleName", column = "middlename"),
            @Result(property = "lastName", column = "lastname")
    })
    User findByEmail(@NotNull @Param("email") String email);

}

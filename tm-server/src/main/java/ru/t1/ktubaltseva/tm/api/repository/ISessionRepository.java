package ru.t1.ktubaltseva.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.model.Session;

import java.util.List;

public interface ISessionRepository {

    @Insert("INSERT INTO tm_session (id, created, user_id, role) " +
            "VALUES (#{id}, #{created}, #{userId}, #{role})")
    @NotNull
    void add(@NotNull Session session);

    @Delete("DELETE FROM tm_session")
    void clear();

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId}")
    void clearByUserId(@NotNull @Param("userId") String userId);

    @NotNull
    @Select("SELECT * FROM tm_session")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    List<Session> findAll();

    @NotNull
    @Select("SELECT * FROM tm_session WHERE user_id = #{userId}")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    List<Session> findAllByUserId(@NotNull @Param("userId") String userId);

    @NotNull
    @Select("SELECT * FROM tm_session ORDER BY #{order}")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    List<Session> findAllSort(@NotNull @Param("order") String order);

    @NotNull
    @Select("SELECT * FROM tm_session WHERE user_id = #{userId} ORDER BY #{order}")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    List<Session> findAllSortByUserId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("order") String order
    );

    @Nullable
    @Select("SELECT * FROM tm_session WHERE id = #{id} LIMIT 1")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    Session findOneById(@NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM tm_session WHERE id = #{id} AND user_id = #{userId} LIMIT 1")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    Session findOneByIdByUserId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @Select("SELECT COUNT(*) FROM tm_session")
    int getSize();

    @Select("SELECT COUNT(*) FROM tm_session WHERE user_id = #{userId}")
    int getSizeByUserId(@NotNull @Param("userId") String userId);

    @Nullable
    @Delete("DELETE FROM tm_session WHERE id = #{id}")
    void removeById(@NotNull @Param("id") String id);

    @Nullable
    @Delete("DELETE FROM tm_session WHERE id = #{id} and user_id = #{userId}")
    void removeByIdByUserId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

}

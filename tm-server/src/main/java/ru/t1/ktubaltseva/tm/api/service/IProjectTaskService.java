package ru.t1.ktubaltseva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.model.Task;

public interface IProjectTaskService {

    @NotNull
    Task bindTaskToProject(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String taskId
    ) throws AbstractException;

    @NotNull
    Task unbindTaskFromProject(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String taskId
    ) throws AbstractException;

    void removeProjectById(
            @Nullable String userId,
            @Nullable String projectId
    ) throws AbstractException;

    void clearProjects(
            @Nullable String userId
    ) throws AbstractException;

}

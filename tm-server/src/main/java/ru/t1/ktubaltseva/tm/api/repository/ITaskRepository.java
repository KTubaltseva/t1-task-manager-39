package ru.t1.ktubaltseva.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    @Insert("INSERT INTO tm_task (id, created, user_id, project_id, name, description, status) " +
            "VALUES (#{id}, #{created}, #{userId}, #{projectId}, #{name}, #{description}, #{status})")
    @NotNull
    void add(@NotNull Task task);

    @Delete("DELETE FROM tm_task")
    void clear();

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId}")
    void clearByUserId(@NotNull @Param("userId") String userId);

    @NotNull
    @Select("SELECT * FROM tm_task")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    List<Task> findAll();

    @NotNull
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    List<Task> findAllByUserId(@NotNull @Param("userId") String userId);

    @NotNull
    @Select("SELECT * FROM tm_task ORDER BY #{order}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    List<Task> findAllSort(@NotNull @Param("order") String order);

    @NotNull
    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY #{order}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    List<Task> findAllSortByUserId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("order") String order
    );

    @Nullable
    @Select("SELECT * FROM tm_task WHERE id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    Task findOneById(@NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM tm_task WHERE id = #{id} AND user_id = #{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    Task findOneByIdByUserId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @Select("SELECT COUNT(*) FROM tm_task")
    int getSize();

    @Select("SELECT COUNT(*) FROM tm_task WHERE user_id = #{userId}")
    int getSizeByUserId(@NotNull @Param("userId") String userId);

    @Nullable
    @Delete("DELETE FROM tm_task WHERE id = #{id}")
    void removeById(@NotNull @Param("id") String id);

    @Nullable
    @Delete("DELETE FROM tm_task WHERE id = #{id} and user_id = #{userId}")
    void removeByIdByUserId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @NotNull
    @Update("UPDATE tm_task " +
            "SET " +
            "project_id = #{projectId}, " +
            "name = #{name}, " +
            "description = #{description}, " +
            "status = #{status} " +
            "WHERE id = #{id} and user_id = #{userId}")
    void update(@NotNull Task task);

    @NotNull
    @Select("SELECT * FROM tm_task WHERE project_id = #{projectId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    List<Task> findAllByProjectId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("projectId") String projectId
    );

    @Delete("DELETE FROM tm_task WHERE project_id = #{projectId}")
    void removeAllByProjectId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("projectId") String projectId
    );

}

package ru.t1.ktubaltseva.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO tm_project (id, created, user_id, name, description, status) " +
            "VALUES (#{id}, #{created}, #{userId}, #{name}, #{description}, #{status})")
    @NotNull
    void add(@NotNull Project project);

    @Delete("DELETE FROM tm_project")
    void clear();

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId}")
    void clearByUserId(@NotNull @Param("userId") String userId);

    @NotNull
    @Select("SELECT * FROM tm_project")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    List<Project> findAll();

    @NotNull
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId}")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    List<Project> findAllByUserId(@NotNull @Param("userId") String userId);

    @NotNull
    @Select("SELECT * FROM tm_project ORDER BY #{order}")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    List<Project> findAllSort(@NotNull @Param("order") String order);

    @NotNull
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY #{order}")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    List<Project> findAllSortByUserId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("order") String order
    );

    @Nullable
    @Select("SELECT * FROM tm_project WHERE id = #{id} LIMIT 1")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    Project findOneById(@NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM tm_project WHERE id = #{id} AND user_id = #{userId} LIMIT 1")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    Project findOneByIdByUserId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @Select("SELECT COUNT(*) FROM tm_project")
    int getSize();

    @Select("SELECT COUNT(*) FROM tm_project WHERE user_id = #{userId}")
    int getSizeByUserId(@NotNull @Param("userId") String userId);

    @Nullable
    @Delete("DELETE FROM tm_project WHERE id = #{id}")
    void removeById(@NotNull @Param("id") String id);

    @Nullable
    @Delete("DELETE FROM tm_project WHERE id = #{id} and user_id = #{userId}")
    void removeByIdByUserId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @NotNull
    @Update("UPDATE tm_project " +
            "SET " +
            "name = #{name}, " +
            "description = #{description}, " +
            "status = #{status} " +
            "WHERE id = #{id} and user_id = #{userId}")
    void update(@NotNull Project project);

}

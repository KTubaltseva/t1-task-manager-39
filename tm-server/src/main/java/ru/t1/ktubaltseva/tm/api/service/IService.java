package ru.t1.ktubaltseva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IService<M extends AbstractModel> {

    @NotNull
    M add(@Nullable M model) throws AbstractException;

    @NotNull
    Collection<M> add(@Nullable Collection<M> models) throws AbstractException;

    @NotNull
    Collection<M> set(@Nullable Collection<M> models) throws AbstractException;

    void clear() throws AbstractException;

    @NotNull
    List<M> findAll() throws AbstractException;

    @NotNull
    List<M> findAll(@Nullable Comparator<M> comparator) throws AbstractException;

    @NotNull
    M findOneById(@Nullable String id) throws AbstractException;

    boolean existsById(@Nullable String id) throws AbstractException;

    @NotNull
    void removeOne(@Nullable M model) throws AbstractException;

    @NotNull
    void removeById(@Nullable String id) throws AbstractException;

    int getSize() throws AbstractException;

}

package ru.t1.ktubaltseva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.api.component.ISaltProvider;

public interface IPropertyService extends ISaltProvider, IDatabaseProperty {

    @NotNull
    String getApplicationConfig();

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    String getGitBranch();

    @NotNull
    String getGitCommitId();

    @NotNull
    String getGitCommitterName();

    @NotNull
    String getGitCommitterEmail();

    @NotNull
    String getGitCommitMessage();

    @NotNull
    String getGitCommitTime();

    @NotNull
    Integer getServerPort();

    @NotNull
    String getServerHost();

    @NotNull
    Integer getSessionTimeout();

    @NotNull
    String getSessionKey();

    @NotNull String getDatabaseDriver();

    @NotNull
    String getMybatisConfig();

}

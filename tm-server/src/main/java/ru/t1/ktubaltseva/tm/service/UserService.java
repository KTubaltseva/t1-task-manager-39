package ru.t1.ktubaltseva.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.repository.IProjectRepository;
import ru.t1.ktubaltseva.tm.api.repository.ISessionRepository;
import ru.t1.ktubaltseva.tm.api.repository.ITaskRepository;
import ru.t1.ktubaltseva.tm.api.repository.IUserRepository;
import ru.t1.ktubaltseva.tm.api.service.IConnectionService;
import ru.t1.ktubaltseva.tm.api.service.IPropertyService;
import ru.t1.ktubaltseva.tm.api.service.IUserService;
import ru.t1.ktubaltseva.tm.comparator.CreatedComparator;
import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.entity.entityAlreadyExists.EmailAlreadyExistsException;
import ru.t1.ktubaltseva.tm.exception.entity.entityAlreadyExists.LoginAlreadyExistsException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.UserNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.*;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.User;
import ru.t1.ktubaltseva.tm.util.HashUtil;

import java.security.NoSuchAlgorithmException;
import java.util.*;

public final class UserService implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final IConnectionService connectionService;

    public UserService(
            @NotNull final IPropertyService propertyService,
            @NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public User add(@Nullable final User user) throws AbstractException {
        if (user == null) throw new UserNotFoundException();
        @Nullable User resultUser;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.add(user);
            sqlSession.commit();
            resultUser = findOneById(user.getId());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return resultUser;
    }

    @NotNull
    @Override
    public Collection<User> add(@Nullable final Collection<User> users) throws AbstractException {
        if (users == null || users.isEmpty()) return Collections.emptyList();
        @NotNull List<User> resultUsers = new ArrayList<>();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            for (@NotNull final User user : users) {
                userRepository.add(user);
            }
            sqlSession.commit();
            for (@NotNull final User user : users) {
                resultUsers.add(findOneById(user.getId()));
            }
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return resultUsers;
    }

    @Override
    public void clear() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.clear();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) throws AbstractException {
        try {
            findOneById(id);
        } catch (UserNotFoundException e) {
            return false;
        }
        return true;
    }

    @NotNull
    @Override
    public List<User> findAll() throws AbstractException {
        @Nullable List<User> resultUsers;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            resultUsers = userRepository.findAll();
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            sqlSession.close();
        }
        return resultUsers;
    }

    @NotNull
    @Override
    public List<User> findAll(@Nullable final Comparator<User> comparator) throws AbstractException {
        if (comparator == null) return findAll();
        @Nullable List<User> resultUsers;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            resultUsers = userRepository.findAllSort(getSortColumnName(comparator));
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            sqlSession.close();
        }
        return resultUsers;
    }

    @NotNull
    @Override
    public User findOneById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable User resultUser;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            resultUser = userRepository.findOneById(id);
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            sqlSession.close();
        }
        if (resultUser == null) throw new UserNotFoundException();
        return resultUser;
    }

    @Override
    public int getSize() throws AbstractException {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        int result = 0;
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            result = userRepository.getSize();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @NotNull
    private String getSortColumnName(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        return "created";
    }

    @NotNull
    @Override
    public Collection<User> set(@Nullable final Collection<User> users) throws AbstractException {
        clear();
        if (users == null || users.isEmpty()) return Collections.emptyList();
        return add(users);
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password
    ) throws AbstractException, NoSuchAlgorithmException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isLoginExists(login)) throw new LoginAlreadyExistsException(login);
        @NotNull User user = new User(login, HashUtil.salt(propertyService, password));
        return add(user);
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws AbstractException, NoSuchAlgorithmException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isLoginExists(login)) throw new LoginAlreadyExistsException(login);
        if (isEmailExists(login)) throw new EmailAlreadyExistsException(email);
        @NotNull User user = new User(login, HashUtil.salt(propertyService, password), email);
        return add(user);
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) throws AbstractException, NoSuchAlgorithmException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        if (isLoginExists(login)) throw new LoginAlreadyExistsException(login);
        @NotNull User user = new User(login, HashUtil.salt(propertyService, password), role);
        return add(user);
    }

    @NotNull
    @Override
    public User findByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User resultUser;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            resultUser = userRepository.findByLogin(login);
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            sqlSession.close();
        }
        if (resultUser == null) throw new UserNotFoundException();
        return resultUser;
    }

    @NotNull
    @Override
    public User findByEmail(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable User resultUser;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            resultUser = userRepository.findByEmail(email);
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            sqlSession.close();
        }
        if (resultUser == null) throw new UserNotFoundException();
        return resultUser;
    }

    @NotNull
    @Override
    public Boolean isLoginExists(@Nullable final String login) throws AbstractException {
        try {
            findByLogin(login);
        } catch (UserNotFoundException e) {
            return false;
        }
        return true;
    }

    @NotNull
    @Override
    public Boolean isEmailExists(@Nullable final String email) throws AbstractException {
        try {
            findByEmail(email);
        } catch (UserNotFoundException e) {
            return false;
        }
        return true;
    }

    @NotNull
    @Override
    public User lockUserByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User resultUser = findByLogin(login);
        resultUser.setLocked(true);
        return update(resultUser);
    }

    @Override
    public void removeOne(@Nullable final User user) throws AbstractException {
        if (user == null) throw new UserNotFoundException();
        removeById(user.getId());
    }

    @Override
    public void removeById(@Nullable final String id) throws AbstractException {
        if (id == null) throw new IdEmptyException();
        if (!existsById(id)) throw new UserNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            @NotNull final List<Project> projects = projectRepository.findAllByUserId(id);
            for (@NotNull final Project project : projects) {
                taskRepository.removeAllByProjectId(id, project.getId());
            }
            projectRepository.clearByUserId(id);
            sessionRepository.clearByUserId(id);
            userRepository.removeById(id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User resultModel = findByLogin(login);
        removeById(resultModel.getId());
    }

    @Override
    public void removeByEmail(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable User resultModel = findByEmail(email);
        removeById(resultModel.getId());
    }

    @NotNull
    @Override
    public User setPassword(
            @Nullable final String id,
            @Nullable final String password
    ) throws AbstractException, NoSuchAlgorithmException {
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable User resultUser = findOneById(id);
        resultUser.setPasswordHash(HashUtil.salt(propertyService, password));
        return update(resultUser);
    }

    @NotNull
    @Override
    public User unlockUserByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User resultUser = findByLogin(login);
        resultUser.setLocked(false);
        return update(resultUser);
    }

    @NotNull
    @Override
    public User update(
            @Nullable final User user
    ) throws AbstractException {
        if (!existsById(user.getId())) throw new UserNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @Nullable User resultUser;
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.update(user);
            sqlSession.commit();
            resultUser = findOneById(user.getId());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return resultUser;
    }

    @NotNull
    @Override
    public User updateById(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String middleName,
            @Nullable final String lastName
    ) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable User resultUser = findOneById(id);
        resultUser.setFirstName(firstName);
        resultUser.setMiddleName(middleName);
        resultUser.setLastName(lastName);
        resultUser = update(resultUser);
        return resultUser;
    }

}

package ru.t1.ktubaltseva.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.Cleanup;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.service.*;
import ru.t1.ktubaltseva.tm.dto.Domain;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.data.LoadDataException;
import ru.t1.ktubaltseva.tm.exception.data.SaveDataException;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DomainService implements IDomainService {

    @NotNull
    public static final String FILE_BASE64 = "./data.base64";

    @NotNull
    public static final String FILE_BINARY = "./data.bin";

    @NotNull
    public static final String FILE_XML = "./data.xml";

    @NotNull
    public static final String FILE_JSON = "./data.json";

    @NotNull
    public static final String FILE_YAML = "./data.yaml";

    @NotNull
    public static final String CONTEXT_FACTORY = "javax.xml.bind.context.factory";

    @NotNull
    public static final String CONTEXT_FACTORY_JAXB = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull
    public static final String MEDIA_TYPE = "eclipselink.media-type";

    @NotNull
    public static final String APPLICATION_TYPE_JSON = "application/json";

    @NotNull
    public static final String FILE_BACKUP = FILE_XML;

    @NotNull
    public static final String COMMAND_BACKUP_LOAD = "data-load-backup";

    @NotNull
    public static final String COMMAND_BACKUP_SAVE = "data-save-backup";

    @NotNull
    private final IServiceLocator serviceLocator;

    @NotNull
    protected IUserService getUserService() {
        return serviceLocator.getUserService();
    }

    @NotNull
    protected IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    @NotNull
    protected IPropertyService getPropertyService() {
        return serviceLocator.getPropertyService();
    }

    @NotNull
    protected IProjectService getProjectService() {
        return serviceLocator.getProjectService();
    }

    @NotNull
    protected ITaskService getTaskService() {
        return serviceLocator.getTaskService();
    }

    public DomainService(@NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    public Domain getDomain() throws AbstractException {
        @NotNull final Domain domain = new Domain();
        domain.setUsers(getUserService().findAll());
        domain.setProjects(getProjectService().findAll());
        domain.setTasks(getTaskService().findAll());
        return domain;
    }

    @Override
    public void setDomain(@Nullable final Domain domain) throws AbstractException {
        if (domain == null) return;
        getUserService().set(domain.getUsers());
        getProjectService().set(domain.getProjects());
        getTaskService().set(domain.getTasks());
    }

    @Override
    public void loadDataBackup() throws LoadDataException {
        loadDataXmlFasterXml();
    }

    @Override
    public void loadDataBase64() throws LoadDataException {
        try {
            @NotNull final byte[] base64Byte = Files.readAllBytes(Paths.get(FILE_BASE64));
            @Nullable final String base64Str = new String(base64Byte);
            @Nullable final byte[] bytes = new BASE64Decoder().decodeBuffer(base64Str);
            @Cleanup @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
            @Cleanup @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
            @NotNull final Domain domain = (Domain) objectInputStream.readObject();
            setDomain(domain);
        } catch (IOException | ClassNotFoundException | AbstractException e) {
            throw new LoadDataException();
        }
    }

    @Override
    public void loadDataBinary() throws LoadDataException {
        try {
            @Cleanup @NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_BINARY);
            @Cleanup @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            @NotNull final Domain domain = (Domain) objectInputStream.readObject();
            setDomain(domain);
        } catch (IOException | ClassNotFoundException | AbstractException e) {
            throw new LoadDataException();
        }
    }

    @Override
    public void loadDataJsonFasterXml() throws LoadDataException {
        try {
            @NotNull final byte[] bytes = Files.readAllBytes(Paths.get(FILE_JSON));
            @Nullable final String json = new String(bytes);
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
            setDomain(domain);
        } catch (IOException | AbstractException e) {
            throw new LoadDataException();
        }
    }

    @Override
    public void loadDataJsonJaxB() throws LoadDataException {
        try {
            System.setProperty(CONTEXT_FACTORY, CONTEXT_FACTORY_JAXB);
            @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
            @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            unmarshaller.setProperty(MEDIA_TYPE, APPLICATION_TYPE_JSON);

            @NotNull final File file = new File(FILE_JSON);
            @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
            setDomain(domain);
        } catch (JAXBException | AbstractException e) {
            throw new LoadDataException();
        }
    }

    @Override
    public void loadDataXmlFasterXml() throws LoadDataException {
        try {
            @NotNull final byte[] bytes = Files.readAllBytes(Paths.get(FILE_XML));
            @Nullable final String xml = new String(bytes);
            @NotNull final ObjectMapper objectMapper = new XmlMapper();
            @NotNull final Domain domain = objectMapper.readValue(xml, Domain.class);
            setDomain(domain);
        } catch (IOException | AbstractException e) {
            throw new LoadDataException();
        }
    }

    @Override
    public void loadDataXmlJaxB() throws LoadDataException {
        try {
            @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
            @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

            @NotNull final File file = new File(FILE_XML);
            @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
            setDomain(domain);
        } catch (JAXBException | AbstractException e) {
            throw new LoadDataException();
        }
    }

    @Override
    public void loadDataYamlFasterXml() throws LoadDataException {
        try {
            @NotNull final byte[] bytes = Files.readAllBytes(Paths.get(FILE_YAML));
            @Nullable final String yaml = new String(bytes);
            @NotNull final ObjectMapper objectMapper = new YAMLMapper();
            @NotNull final Domain domain = objectMapper.readValue(yaml, Domain.class);
            setDomain(domain);
        } catch (IOException | AbstractException e) {
            throw new LoadDataException();
        }
    }

    @Override
    public void saveDataBackup() throws SaveDataException {
        saveDataXmlFasterXml();
    }

    @Override
    public void saveDataBase64() throws SaveDataException {
        try {
            @NotNull final Domain domain = getDomain();
            @NotNull final File file = new File(FILE_BASE64);
            @NotNull final Path path = file.toPath();

            Files.deleteIfExists(path);
            Files.createFile(path);

            @Cleanup @NotNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            @Cleanup @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream.writeObject(domain);

            @Nullable final byte[] bytes = byteArrayOutputStream.toByteArray();
            @Nullable final String base64Str = new BASE64Encoder().encode(bytes);

            @Cleanup @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(base64Str.getBytes());
            fileOutputStream.flush();
        } catch (IOException | AbstractException e) {
            throw new SaveDataException();
        }
    }

    @Override
    public void saveDataBinary() throws SaveDataException {
        try {
            @NotNull final Domain domain = getDomain();
            @NotNull final File file = new File(FILE_BINARY);
            @NotNull final Path path = file.toPath();

            Files.deleteIfExists(path);
            Files.createFile(path);

            @Cleanup @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
            @Cleanup @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(domain);
            objectOutputStream.flush();
        } catch (IOException | AbstractException e) {
            throw new SaveDataException();
        }
    }

    @Override
    public void saveDataJsonFasterXml() throws SaveDataException {
        try {
            @NotNull final Domain domain = getDomain();
            @NotNull final File file = new File(FILE_JSON);
            @NotNull final Path path = file.toPath();

            Files.deleteIfExists(path);
            Files.createFile(path);

            @Cleanup @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
            fileOutputStream.write(json.getBytes());
            fileOutputStream.flush();
        } catch (IOException | AbstractException e) {
            throw new SaveDataException();
        }
    }

    @Override
    public void saveDataJsonJaxB() throws SaveDataException {
        try {
            System.setProperty(CONTEXT_FACTORY, CONTEXT_FACTORY_JAXB);
            @NotNull final Domain domain = getDomain();
            @NotNull final File file = new File(FILE_JSON);
            @NotNull final Path path = file.toPath();

            Files.deleteIfExists(path);
            Files.createFile(path);

            @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
            @NotNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.setProperty(MEDIA_TYPE, APPLICATION_TYPE_JSON);

            @Cleanup @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
            jaxbMarshaller.marshal(domain, fileOutputStream);
            fileOutputStream.flush();
        } catch (IOException | JAXBException | AbstractException e) {
            throw new SaveDataException();
        }
    }

    @Override
    public void saveDataXmlFasterXml() throws SaveDataException {
        try {
            @NotNull final Domain domain = getDomain();
            @NotNull final File file = new File(FILE_XML);
            @NotNull final Path path = file.toPath();

            Files.deleteIfExists(path);
            Files.createFile(path);

            @Cleanup @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
            @NotNull final ObjectMapper objectMapper = new XmlMapper();
            @NotNull final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
            fileOutputStream.write(xml.getBytes());
            fileOutputStream.flush();
        } catch (IOException | AbstractException e) {
            throw new SaveDataException();
        }
    }

    @Override
    public void saveDataXmlJaxB() throws SaveDataException {
        try {
            @NotNull final Domain domain = getDomain();
            @NotNull final File file = new File(FILE_XML);
            @NotNull final Path path = file.toPath();

            Files.deleteIfExists(path);
            Files.createFile(path);

            @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
            @NotNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            @Cleanup @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
            jaxbMarshaller.marshal(domain, fileOutputStream);
            fileOutputStream.flush();
        } catch (IOException | JAXBException | AbstractException e) {
            throw new SaveDataException();
        }
    }

    @Override
    public void saveDataYamlFasterXml() throws SaveDataException {
        try {
            @NotNull final Domain domain = getDomain();
            @NotNull final File file = new File(FILE_YAML);
            @NotNull final Path path = file.toPath();

            Files.deleteIfExists(path);
            Files.createFile(path);

            @Cleanup @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
            @NotNull final ObjectMapper objectMapper = new YAMLMapper();
            @NotNull final String yaml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
            fileOutputStream.write(yaml.getBytes());
            fileOutputStream.flush();
        } catch (IOException | AbstractException e) {
            throw new SaveDataException();
        }
    }

}

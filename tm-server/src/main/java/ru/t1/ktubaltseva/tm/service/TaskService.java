package ru.t1.ktubaltseva.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.repository.ITaskRepository;
import ru.t1.ktubaltseva.tm.api.service.IConnectionService;
import ru.t1.ktubaltseva.tm.api.service.ITaskService;
import ru.t1.ktubaltseva.tm.comparator.CreatedComparator;
import ru.t1.ktubaltseva.tm.comparator.NameComparator;
import ru.t1.ktubaltseva.tm.comparator.StatusComparator;
import ru.t1.ktubaltseva.tm.enumerated.Sort;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.TaskNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.*;
import ru.t1.ktubaltseva.tm.model.Task;

import java.util.*;

public final class TaskService implements ITaskService {

    @NotNull
    private final IConnectionService connectionService;

    public TaskService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    public Task add(@Nullable final Task task) throws AbstractException {
        if (task == null) throw new TaskNotFoundException();
        @Nullable Task resultTask;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.add(task);
            sqlSession.commit();
            resultTask = findOneById(task.getId());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return resultTask;
    }

    @NotNull
    @Override
    public Task add(
            @Nullable final String userId,
            @Nullable final Task task
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (task == null) throw new TaskNotFoundException();
        task.setUserId(userId);
        @Nullable Task resultTask;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.add(task);
            sqlSession.commit();
            resultTask = findOneById(task.getId());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return resultTask;
    }

    @NotNull
    @Override
    public Collection<Task> add(@Nullable final Collection<Task> tasks) throws AbstractException {
        if (tasks == null || tasks.isEmpty()) return Collections.emptyList();
        @NotNull List<Task> resultTasks = new ArrayList<>();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            for (@NotNull final Task task : tasks) {
                taskRepository.add(task);
            }
            sqlSession.commit();
            for (@NotNull final Task task : tasks) {
                resultTasks.add(findOneById(task.getId()));
            }
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return resultTasks;
    }

    @Override
    public void clear() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.clear();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void clear(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.clearByUserId(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) throws AbstractException {
        try {
            findOneById(id);
        } catch (TaskNotFoundException e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        try {
            findOneById(userId, id);
        } catch (TaskNotFoundException e) {
            return false;
        }
        return true;
    }

    @NotNull
    @Override
    public List<Task> findAll() throws AbstractException {
        @Nullable List<Task> resultTasks;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            resultTasks = taskRepository.findAll();
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            sqlSession.close();
        }
        return resultTasks;
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        @Nullable List<Task> resultTasks;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            resultTasks = taskRepository.findAllByUserId(userId);
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            sqlSession.close();
        }
        return resultTasks;
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final Comparator<Task> comparator) throws AbstractException {
        if (comparator == null) return findAll();
        @Nullable List<Task> resultTasks;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            resultTasks = taskRepository.findAllSort(getSortColumnName(comparator));
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            sqlSession.close();
        }
        return resultTasks;
    }

    @NotNull
    @Override
    public List<Task> findAll(
            @Nullable final String userId,
            @Nullable final Comparator<Task> comparator
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (comparator == null) return findAll(userId);
        @Nullable List<Task> resultTasks;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            resultTasks = taskRepository.findAllSortByUserId(userId, getSortColumnName(comparator));
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            sqlSession.close();
        }
        return resultTasks;
    }

    @NotNull
    @Override
    public List<Task> findAll(
            @Nullable final String userId,
            @Nullable final Sort sort
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (sort == null) return findAll(userId);
        @Nullable List<Task> resultTasks;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            resultTasks = taskRepository.findAllSortByUserId(userId, getSortColumnName(sort.getComparator()));
            sqlSession.commit();
        } finally {
            sqlSession.close();
        }
        return resultTasks;
    }

    @NotNull
    @Override
    public Task findOneById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Task resultTask;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            resultTask = taskRepository.findOneById(id);
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            sqlSession.close();
        }
        if (resultTask == null) throw new TaskNotFoundException();
        return resultTask;
    }

    @NotNull
    @Override
    public Task findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Task resultTask;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            resultTask = taskRepository.findOneByIdByUserId(userId, id);
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            sqlSession.close();
        }
        if (resultTask == null) throw new TaskNotFoundException();
        return resultTask;
    }

    @Override
    public int getSize() throws AbstractException {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        int result = 0;
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            result = taskRepository.getSize();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return result;
    }

    @NotNull
    private String getSortColumnName(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        if (comparator == NameComparator.INSTANCE) return "name";
        if (comparator == StatusComparator.INSTANCE) return "status";
        return "created";
    }

    @Override
    public void removeOne(@Nullable final Task task) throws AbstractException {
        if (task == null) throw new TaskNotFoundException();
        removeById(task.getId());
    }

    @Override
    public void removeOne(
            @Nullable final String userId,
            @Nullable final Task task
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (task == null) throw new TaskNotFoundException();
        removeById(userId, task.getId());
    }

    @Override
    public void removeById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (!existsById(id)) throw new TaskNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.removeById(id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (!existsById(userId, id)) throw new TaskNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.removeByIdByUserId(userId, id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public Collection<Task> set(@Nullable final Collection<Task> tasks) throws AbstractException {
        clear();
        if (tasks == null || tasks.isEmpty()) return Collections.emptyList();
        return add(tasks);
    }

    @NotNull
    public Task update(
            @Nullable final String userId,
            @Nullable final Task task
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (!existsById(userId, task.getId())) throw new TaskNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @Nullable Task resultTask;
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.update(task);
            sqlSession.commit();
            resultTask = findOneById(task.getId());
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return resultTask;
    }

    @NotNull
    @Override
    public Task changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable Task resultTask = findOneById(userId, id);
        resultTask.setStatus(status);
        resultTask = update(userId, resultTask);
        return resultTask;
    }

    @NotNull
    @Override
    public Task create(
            @Nullable final String userId,
            @Nullable final String name
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull Task task = new Task(name);
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull Task task = new Task(name, description);
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    public Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable Task resultTask = findOneById(userId, id);
        resultTask.setName(name);
        resultTask.setDescription(description);
        resultTask = update(userId, resultTask);
        return resultTask;
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable String projectId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @Nullable List<Task> resultModels;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            resultModels = taskRepository.findAllByProjectId(userId, projectId);
        } catch (@NotNull final Exception e) {
            throw e;
        } finally {
            sqlSession.close();
        }
        return resultModels;
    }

}

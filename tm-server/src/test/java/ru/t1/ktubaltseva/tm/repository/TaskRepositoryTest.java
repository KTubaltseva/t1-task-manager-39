package ru.t1.ktubaltseva.tm.repository;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.ktubaltseva.tm.api.repository.ITaskRepository;
import ru.t1.ktubaltseva.tm.api.service.*;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.Task;
import ru.t1.ktubaltseva.tm.model.User;
import ru.t1.ktubaltseva.tm.service.*;

import java.util.Collection;
import java.util.Collections;

import static ru.t1.ktubaltseva.tm.constant.TaskRepTestData.*;

@Category(UnitCategory.class)
public final class TaskRepositoryTest {

    @NotNull
    private static final ILoggerService loggerService = new LoggerService();

    @NotNull
    private static final IPropertyService propertyService = new PropertyService(loggerService);

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final SqlSession sqlSession = connectionService.getSqlSession();

    @NotNull
    private final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);

    @NotNull
    private static final IUserService userService = new UserService(propertyService, connectionService);

    @NotNull
    private static final IProjectService projectService = new ProjectService(connectionService);

    @BeforeClass
    @SneakyThrows
    public static void before() {
        userService.add(USER_1);
        userService.add(USER_2);
        sqlSession.commit();
        projectService.add(USER_1_PROJECT_1);
        projectService.add(USER_1_PROJECT_2);
        sqlSession.commit();
    }

    @AfterClass
    @SneakyThrows
    public static void afterClazz() {
        projectService.clear(USER_1.getId());
        projectService.clear(USER_2.getId());
        sqlSession.commit();
        userService.removeOne(USER_1);
        userService.removeOne(USER_2);
        sqlSession.commit();
        sqlSession.close();
    }

    @After
    @SneakyThrows
    public void after() {
        sqlSession.commit();
        for (@NotNull final Task task : TASK_LIST) {
            try {
                repository.removeById(task.getId());
            } catch (@NotNull final Exception e) {

            }
        }
        repository.clearByUserId(USER_1.getId());
        repository.clearByUserId(USER_2.getId());
        sqlSession.commit();
    }

    @Test
    @SneakyThrows
    public void add() {
        @Nullable final Task taskToAdd = USER_1_TASK_1;
        @Nullable final String taskToAddId = taskToAdd.getId();
        repository.add((taskToAdd));
        @Nullable final Task taskFindOneById = repository.findOneById(taskToAddId);
        Assert.assertNotNull(taskFindOneById);
        Assert.assertEquals(taskToAdd.getId(), taskFindOneById.getId());
    }

    @Test
    @SneakyThrows
    public void findOneById() {
        @NotNull final Task taskExists = USER_1_TASK_1;
        repository.add(taskExists);

        @Nullable final Task taskFindOneById = repository.findOneById(taskExists.getId());
        Assert.assertNotNull(taskFindOneById);
        Assert.assertEquals(taskExists.getId(), taskFindOneById.getId());

        @Nullable final Task taskFindOneByIdNonExistent = repository.findOneById(NON_EXISTENT_TASK_ID);
        Assert.assertNull(taskFindOneByIdNonExistent);
    }

    @Test
    @SneakyThrows
    public void findOneByIdByUserId() {
        @NotNull final Task taskExists = USER_1_TASK_1;
        repository.add(taskExists);

        Assert.assertNull(repository.findOneByIdByUserId(taskExists.getUserId(), NON_EXISTENT_TASK_ID));
        Assert.assertNull(repository.findOneByIdByUserId(NON_EXISTENT_USER_ID, taskExists.getId()));

        @Nullable final Task taskFindOneById = repository.findOneByIdByUserId(taskExists.getUserId(), taskExists.getId());
        Assert.assertNotNull(taskFindOneById);
        Assert.assertEquals(taskExists.getId(), taskFindOneById.getId());
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final Task taskExists = USER_1_TASK_1;

        repository.add(taskExists);
        @NotNull final Collection<Task> tasksFindAllNoEmpty = repository.findAll();
        Assert.assertNotNull(tasksFindAllNoEmpty);
    }

    @Test
    @SneakyThrows
    public void findAllByUserId() {
        @NotNull final Task taskExists = USER_1_TASK_1;

        repository.add(taskExists);
        @NotNull final Collection<Task> tasksFindAllByUserRepNoEmpty = repository.findAllByUserId(taskExists.getUserId());
        Assert.assertNotNull(tasksFindAllByUserRepNoEmpty);

        @NotNull final Collection<Task> tasksFindAllByNonExistentUser = repository.findAllByUserId(NON_EXISTENT_USER_ID);
        Assert.assertNotNull(tasksFindAllByNonExistentUser);
        Assert.assertEquals(Collections.emptyList(), tasksFindAllByNonExistentUser);
    }

    @Test
    @SneakyThrows
    public void findAllByProjectId() {
        @NotNull final Task taskExists = USER_1_TASK_1;
        @NotNull final Project projectExists = USER_1_PROJECT_1;

        repository.add(taskExists);
        @NotNull final Collection<Task> tasksFindAllByUserEmptyProject = repository.findAllByProjectId(taskExists.getUserId(), projectExists.getId());
        Assert.assertNotNull(tasksFindAllByUserEmptyProject);
        Assert.assertEquals(Collections.emptyList(), tasksFindAllByUserEmptyProject);

        taskExists.setProjectId(projectExists.getId());
        @NotNull final Collection<Task> tasksFindAllByUserBindProject = repository.findAllByProjectId(taskExists.getUserId(), projectExists.getId());
        Assert.assertNotNull(tasksFindAllByUserBindProject);

        @NotNull final Collection<Task> tasksFindAllByNonExistentUser = repository.findAllByProjectId(NON_EXISTENT_USER_ID, projectExists.getId());
        Assert.assertNotNull(tasksFindAllByNonExistentUser);
        Assert.assertEquals(Collections.emptyList(), tasksFindAllByNonExistentUser);

        @NotNull final Collection<Task> tasksFindAllByNonExistentProject = repository.findAllByProjectId(taskExists.getUserId(), NON_EXISTENT_PROJECT_ID);
        Assert.assertNotNull(tasksFindAllByNonExistentProject);
        Assert.assertEquals(Collections.emptyList(), tasksFindAllByNonExistentProject);
    }

    @Test
    @SneakyThrows
    public void clearByUserId() {
        @NotNull final User userToClear = USER_1;
        @NotNull final User userNoClear = USER_2;
        @NotNull final String userToClearId = userToClear.getId();
        @NotNull final String userNoClearId = userNoClear.getId();
        @NotNull final Task taskByUserToClear = USER_1_TASK_1;
        repository.add(taskByUserToClear);
        @NotNull final Task taskByUserNoClear = USER_2_TASK_1;
        repository.add(taskByUserNoClear);
        repository.clearByUserId(userToClearId);

        @Nullable final Task taskFindOneByIdToClear = repository.findOneById(taskByUserToClear.getId());
        Assert.assertNull(taskFindOneByIdToClear);
        Assert.assertEquals(0, repository.findAllByUserId(userToClearId).size());

        @Nullable final Task taskFindOneByIdNoClear = repository.findOneById(taskByUserNoClear.getId());
        Assert.assertEquals(taskByUserNoClear.getId(), taskFindOneByIdNoClear.getId());
        Assert.assertNotEquals(0, repository.findAllByUserId(userNoClearId).size());
    }

    @Test
    @SneakyThrows
    public void removeById() {
        @Nullable final Task taskToRemove = USER_1_TASK_1;
        repository.add((taskToRemove));

        repository.removeById(taskToRemove.getId());
        @Nullable final Task taskFindOneById = repository.findOneById(taskToRemove.getId());
        Assert.assertNull(taskFindOneById);
    }

    @Test
    @SneakyThrows
    public void removeByIdByUserId() {
        @NotNull final User userToRemove = USER_1;
        @NotNull final String userToRemoveId = userToRemove.getId();
        @Nullable final Task taskByUserToRemove = USER_1_TASK_1;
        repository.add((taskByUserToRemove));
        @Nullable final Task taskByUserNoRemove = USER_2_TASK_1;
        repository.add((taskByUserNoRemove));

        repository.removeByIdByUserId(userToRemoveId, taskByUserToRemove.getId());
        @Nullable final Task taskRemovedFindOneById = repository.findOneById(taskByUserToRemove.getId());
        Assert.assertNull(taskRemovedFindOneById);

        repository.removeByIdByUserId(userToRemoveId, taskByUserNoRemove.getId());
        @Nullable final Task taskNoRemovedFindOneById = repository.findOneById(taskByUserNoRemove.getId());
        Assert.assertNotNull(taskNoRemovedFindOneById);
        Assert.assertEquals(taskNoRemovedFindOneById.getId(), taskByUserNoRemove.getId());
    }

    @Test
    @SneakyThrows
    public void removeAllByIdByProjectId() {
        @NotNull final User userToRemove = USER_1;
        @NotNull final Project projectToRemove = USER_1_PROJECT_1;
        @NotNull final Project projectNoRemove = USER_1_PROJECT_2;
        @Nullable final Task taskByProjectToRemove = USER_1_TASK_1;
        @Nullable final Task taskByProjectNoRemove = USER_1_TASK_2;

        taskByProjectToRemove.setProjectId(projectToRemove.getId());
        taskByProjectNoRemove.setProjectId(projectNoRemove.getId());
        repository.add(taskByProjectToRemove);
        repository.add(taskByProjectNoRemove);
        repository.removeAllByProjectId(userToRemove.getId(), projectToRemove.getId());

        @Nullable final Task taskRemovedFindOneById = repository.findOneById(taskByProjectToRemove.getId());
        Assert.assertNull(taskRemovedFindOneById);

        @Nullable final Task taskNoRemovedFindOneById = repository.findOneById(taskByProjectNoRemove.getId());
        Assert.assertNotNull(taskNoRemovedFindOneById);
        Assert.assertEquals(taskNoRemovedFindOneById.getId(), taskByProjectNoRemove.getId());
    }

}

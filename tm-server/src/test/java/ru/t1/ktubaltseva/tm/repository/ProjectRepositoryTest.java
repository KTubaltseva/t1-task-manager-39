package ru.t1.ktubaltseva.tm.repository;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.ktubaltseva.tm.api.repository.IProjectRepository;
import ru.t1.ktubaltseva.tm.api.service.IConnectionService;
import ru.t1.ktubaltseva.tm.api.service.ILoggerService;
import ru.t1.ktubaltseva.tm.api.service.IPropertyService;
import ru.t1.ktubaltseva.tm.api.service.IUserService;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.User;
import ru.t1.ktubaltseva.tm.service.ConnectionService;
import ru.t1.ktubaltseva.tm.service.LoggerService;
import ru.t1.ktubaltseva.tm.service.PropertyService;
import ru.t1.ktubaltseva.tm.service.UserService;

import java.util.Collection;

import static ru.t1.ktubaltseva.tm.constant.ProjectTestData.*;

@Category(UnitCategory.class)
public final class ProjectRepositoryTest {

    @NotNull
    private static final ILoggerService loggerService = new LoggerService();

    @NotNull
    private static final IPropertyService propertyService = new PropertyService(loggerService);

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final SqlSession sqlSession = connectionService.getSqlSession();

    @NotNull
    private final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);

    @NotNull
    private static final IUserService userService = new UserService(propertyService, connectionService);

    @BeforeClass
    @SneakyThrows
    public static void before() {
        userService.add(USER_1);
        userService.add(USER_2);
        sqlSession.commit();
    }

    @AfterClass
    @SneakyThrows
    public static void afterClazz() {
        userService.removeOne(USER_1);
        userService.removeOne(USER_2);
        sqlSession.commit();
        sqlSession.close();
    }

    @After
    @SneakyThrows
    public void after() {
        sqlSession.commit();
        for (@NotNull final Project project : PROJECT_LIST) {
            repository.removeById(project.getId());
        }
        repository.clearByUserId(USER_1.getId());
        repository.clearByUserId(USER_2.getId());
        sqlSession.commit();
    }

    @Test
    @SneakyThrows
    public void add() {
        @Nullable final Project projectToAdd = USER_1_PROJECT_1;
        @Nullable final String projectToAddId = projectToAdd.getId();

        repository.add((projectToAdd));
        sqlSession.commit();
        @Nullable final Project projectFindOneById = repository.findOneById(projectToAddId);
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(projectToAdd.getId(), projectFindOneById.getId());
    }

    @Test
    @SneakyThrows
    public void findOneById() {
        @NotNull final Project projectExists = USER_1_PROJECT_1;
        repository.add(projectExists);
        sqlSession.commit();

        @Nullable final Project projectFindOneById = repository.findOneById(projectExists.getId());
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(projectExists.getId(), projectFindOneById.getId());

        @Nullable final Project projectFindOneByIdNonExistent = repository.findOneById(NON_EXISTENT_PROJECT_ID);
        Assert.assertNull(projectFindOneByIdNonExistent);
    }

    @Test
    @SneakyThrows
    public void findOneByIdByUserId() {
        @NotNull final Project projectExists = USER_1_PROJECT_1;
        repository.add(projectExists);
        sqlSession.commit();

        Assert.assertNull(repository.findOneByIdByUserId(projectExists.getUserId(), NON_EXISTENT_PROJECT_ID));
        Assert.assertNull(repository.findOneByIdByUserId(NON_EXISTENT_USER_ID, projectExists.getId()));

        @Nullable final Project projectFindOneById = repository.findOneByIdByUserId(projectExists.getUserId(), projectExists.getId());
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(projectExists.getId(), projectFindOneById.getId());
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final Project projectExists = USER_1_PROJECT_1;
        repository.add(projectExists);
        sqlSession.commit();

        @NotNull final Collection<Project> projectsFindAllNoEmpty = repository.findAll();
        Assert.assertNotNull(projectsFindAllNoEmpty);
    }

    @Test
    @SneakyThrows
    public void findAllByUserId() {
        @NotNull final Project projectExists = USER_1_PROJECT_1;
        @NotNull final User userExists = USER_1;
        repository.add(projectExists);
        sqlSession.commit();

        @NotNull final Collection<Project> projectsFindAllByUserRepNoEmpty = repository.findAllByUserId(userExists.getId());
        Assert.assertNotNull(projectsFindAllByUserRepNoEmpty);
    }

    @Test
    @SneakyThrows
    public void clearByUserId() {
        @NotNull final User userToClear = USER_1;
        @NotNull final User userNoClear = USER_2;
        @NotNull final String userToClearId = userToClear.getId();
        @NotNull final String userNoClearId = userNoClear.getId();
        @NotNull final Project projectByUserToClear = USER_1_PROJECT_1;
        repository.add(projectByUserToClear);
        @NotNull final Project projectByUserNoClear = USER_2_PROJECT_2;
        repository.add(projectByUserNoClear);
        sqlSession.commit();

        repository.clearByUserId(userToClearId);
        sqlSession.commit();

        @Nullable final Project projectFindOneByIdToClear = repository.findOneById(projectByUserToClear.getId());
        Assert.assertNull(projectFindOneByIdToClear);
        Assert.assertEquals(0, repository.findAllByUserId(userToClearId).size());

        @Nullable final Project projectFindOneByIdNoClear = repository.findOneById(projectByUserNoClear.getId());
        Assert.assertEquals(projectByUserNoClear.getId(), projectFindOneByIdNoClear.getId());
        Assert.assertNotEquals(0, repository.findAllByUserId(userNoClearId).size());
    }

    @Test
    @SneakyThrows
    public void removeById() {
        @Nullable final Project projectToRemove = USER_1_PROJECT_1;
        repository.add((projectToRemove));
        sqlSession.commit();

        repository.removeById(projectToRemove.getId());
        @Nullable final Project projectFindOneById = repository.findOneById(projectToRemove.getId());
        Assert.assertNull(projectFindOneById);
    }

    @Test
    @SneakyThrows
    public void removeByIdByUserId() {
        @NotNull final User userToRemove = USER_1;
        @NotNull final String userToRemoveId = userToRemove.getId();
        @Nullable final Project projectByUserToRemove = USER_1_PROJECT_1;
        repository.add((projectByUserToRemove));
        @Nullable final Project projectByUserNoRemove = USER_2_PROJECT_1;
        repository.add((projectByUserNoRemove));
        sqlSession.commit();

        repository.removeByIdByUserId(userToRemoveId, projectByUserToRemove.getId());
        sqlSession.commit();
        @Nullable final Project projectRemovedFindOneById = repository.findOneById(projectByUserToRemove.getId());
        Assert.assertNull(projectRemovedFindOneById);

        repository.removeByIdByUserId(userToRemoveId, projectByUserNoRemove.getId());
        sqlSession.commit();
        @Nullable final Project projectNoRemovedFindOneById = repository.findOneById(projectByUserNoRemove.getId());
        Assert.assertNotNull(projectNoRemovedFindOneById);
        Assert.assertEquals(projectNoRemovedFindOneById.getId(), projectByUserNoRemove.getId());
    }

}

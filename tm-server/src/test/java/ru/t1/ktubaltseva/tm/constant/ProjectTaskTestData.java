package ru.t1.ktubaltseva.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.Task;
import ru.t1.ktubaltseva.tm.model.User;

import java.util.Arrays;
import java.util.List;

@UtilityClass
public final class ProjectTaskTestData {

    @Nullable
    public final static String USER_1_LOGIN = "USER_1_LOGIN";

    @Nullable
    public final static String USER_1_PASSWORD = "USER_1_PASSWORD";

    @Nullable
    public final static String USER_1_EMAIL = "USER_1_EMAIL";

    @Nullable
    public final static String USER_2_LOGIN = "USER_2_LOGIN";

    @Nullable
    public final static String USER_2_PASSWORD = "USER_2_PASSWORD";

    @Nullable
    public final static String USER_2_EMAIL = "USER_2_EMAIL";

    @NotNull
    public final static User USER_1 = new User(USER_1_LOGIN, USER_1_PASSWORD, USER_1_EMAIL);

    @NotNull
    public final static User USER_2 = new User(USER_2_LOGIN, USER_2_PASSWORD, USER_2_EMAIL);

    @Nullable
    public final static Task NULL_TASK = null;

    @Nullable
    public final static String NULL_USER_ID = null;

    @Nullable
    public final static String NON_EXISTENT_USER_ID = "NON_EXISTENT_USER_ID";

    @Nullable
    public final static String NULL_TASK_ID = null;

    @Nullable
    public final static String NON_EXISTENT_TASK_ID = "NON_EXISTENT_TASK_ID";

    @Nullable
    public final static String NULL_PROJECT_ID = null;

    @Nullable
    public final static String NON_EXISTENT_PROJECT_ID = "NON_EXISTENT_PROJECT_ID";

    @Nullable
    public final static Task NON_EXISTENT_TASK = new Task();

    @NotNull
    public final static Task USER_1_TASK_1 = new Task(USER_1, "name1");

    @NotNull
    public final static Task USER_1_TASK_2 = new Task(USER_1, "name2");

    @NotNull
    public final static Task USER_2_TASK_1 = new Task(USER_2, "name3");

    @NotNull
    public final static Task USER_2_TASK_2 = new Task(USER_2, "name4");

    @NotNull
    public final static Project USER_1_PROJECT_1 = new Project(USER_1, "name1");

    @NotNull
    public final static Project USER_1_PROJECT_2 = new Project(USER_1, "name2");

    @NotNull
    public final static List<Task> USER_1_TASK_LIST = Arrays.asList(USER_1_TASK_1, USER_1_TASK_2);

    @NotNull
    public final static List<Task> USER_2_TASK_LIST = Arrays.asList(USER_2_TASK_1, USER_2_TASK_2);

    @NotNull
    public final static List<Task> TASK_LIST = Arrays.asList(USER_1_TASK_1, USER_1_TASK_2, USER_2_TASK_1, USER_2_TASK_2);

    @Nullable
    public final static String NULL_NAME = null;

    @Nullable
    public final static String NULL_DESC = null;

    @Nullable
    public final static String TASK_NAME = "TASK_NAME";

    @Nullable
    public final static String TASK_DESC = "TASK_DESC";

    @Nullable
    public final static Status NULL_STATUS = null;

    @Nullable
    public final static Status TASK_STATUS = Status.COMPLETED;

}

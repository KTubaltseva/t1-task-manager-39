package ru.t1.ktubaltseva.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.model.Session;
import ru.t1.ktubaltseva.tm.model.User;

import java.util.Arrays;
import java.util.List;

@UtilityClass
public final class SessionTestData {

    @Nullable
    public final static String USER_1_LOGIN = "USER_1_LOGIN";

    @Nullable
    public final static String USER_1_PASSWORD = "USER_1_PASSWORD";

    @Nullable
    public final static String USER_1_EMAIL = "USER_1_EMAIL";

    @Nullable
    public final static String USER_2_LOGIN = "USER_2_LOGIN";

    @Nullable
    public final static String USER_2_PASSWORD = "USER_2_PASSWORD";

    @Nullable
    public final static String USER_2_EMAIL = "USER_2_EMAIL";

    @NotNull
    public final static User USER_1 = new User(USER_1_LOGIN, USER_1_PASSWORD, USER_1_EMAIL);

    @NotNull
    public final static User USER_2 = new User(USER_2_LOGIN, USER_2_PASSWORD, USER_2_EMAIL);

    @Nullable
    public final static Session NULL_SESSION = null;

    @Nullable
    public final static String NULL_USER_ID = null;

    @Nullable
    public final static String NON_EXISTENT_USER_ID = "NON_EXISTENT_USER_ID";

    @Nullable
    public final static String NULL_SESSION_ID = null;

    @Nullable
    public final static String NON_EXISTENT_SESSION_ID = "NON_EXISTENT_SESSION_ID";

    @Nullable
    public final static Session NON_EXISTENT_SESSION = new Session();

    @Nullable
    public final static String NULL_NAME = null;

    @Nullable
    public final static String NULL_DESC = null;

    @NotNull
    public final static Session USER_1_SESSION_1 = new Session(USER_1);

    @NotNull
    public final static Session USER_1_SESSION_2 = new Session(USER_1);

    @NotNull
    public final static Session USER_2_SESSION_1 = new Session(USER_2);

    @NotNull
    public final static Session USER_2_SESSION_2 = new Session(USER_2);

    @NotNull
    public final static List<Session> USER_1_SESSION_LIST = Arrays.asList(USER_1_SESSION_1, USER_1_SESSION_2);

    @NotNull
    public final static List<Session> USER_2_SESSION_LIST = Arrays.asList(USER_2_SESSION_1, USER_2_SESSION_2);

    @NotNull
    public final static List<Session> SESSION_LIST = Arrays.asList(USER_1_SESSION_1, USER_1_SESSION_2, USER_2_SESSION_1, USER_2_SESSION_2);

    @Nullable
    public final static String SESSION_NAME = "SESSION_NAME";

    @Nullable
    public final static String SESSION_DESC = "SESSION_DESC";

}

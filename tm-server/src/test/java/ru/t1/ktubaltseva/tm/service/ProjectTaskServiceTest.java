package ru.t1.ktubaltseva.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.ktubaltseva.tm.api.service.*;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.ProjectNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.TaskNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.ProjectIdEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.TaskIdEmptyException;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.Task;
import ru.t1.ktubaltseva.tm.model.User;

import static ru.t1.ktubaltseva.tm.constant.ProjectTaskTestData.*;

@Category(UnitCategory.class)
public final class ProjectTaskServiceTest {

    @NotNull
    private static final ILoggerService loggerService = new LoggerService();

    @NotNull
    private static final IPropertyService propertyService = new PropertyService(loggerService);

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final SqlSession sqlSession = connectionService.getSqlSession();

    @NotNull
    private final IProjectTaskService service = new ProjectTaskService(connectionService);

    @NotNull
    private static final IUserService userService = new UserService(propertyService, connectionService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @BeforeClass
    @SneakyThrows
    public static void before() {
        userService.add(USER_1);
        userService.add(USER_2);
    }

    @AfterClass
    @SneakyThrows
    public static void afterClazz() {
        userService.removeOne(USER_1);
        userService.removeOne(USER_2);
        sqlSession.close();
    }

    @After
    @SneakyThrows
    public void after() {
        taskService.clear(USER_1.getId());
        taskService.clear(USER_2.getId());
        projectService.clear(USER_1.getId());
        projectService.clear(USER_2.getId());
    }

    @Test
    @SneakyThrows
    public void bindTaskToProject() throws AbstractException {
        @Nullable final User user = USER_1;
        @Nullable final Task task = USER_1_TASK_1;
        @Nullable final Project project = USER_1_PROJECT_1;
        projectService.add(project);
        taskService.add(task);

        Assert.assertThrows(AuthRequiredException.class, () -> service.bindTaskToProject(NULL_USER_ID, project.getId(), task.getId()));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.bindTaskToProject(NON_EXISTENT_USER_ID, project.getId(), task.getId()));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.bindTaskToProject(user.getId(), NULL_PROJECT_ID, task.getId()));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.bindTaskToProject(user.getId(), NON_EXISTENT_PROJECT_ID, task.getId()));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.bindTaskToProject(user.getId(), project.getId(), NULL_TASK_ID));
        Assert.assertThrows(TaskNotFoundException.class, () -> service.bindTaskToProject(user.getId(), project.getId(), NON_EXISTENT_TASK_ID));

        @Nullable final Task taskBinded = service.bindTaskToProject(user.getId(), project.getId(), task.getId());
        Assert.assertNotNull(taskBinded);
        Assert.assertEquals(task.getId(), taskBinded.getId());
        Assert.assertEquals(project.getId(), task.getProjectId());
    }

    @Test
    @SneakyThrows
    public void unbindTaskFromProject() throws AbstractException {
        @Nullable final User user = USER_1;
        @Nullable final Task task = USER_1_TASK_1;
        @Nullable final Project project = USER_1_PROJECT_1;
        task.setProjectId(project.getId());
        projectService.add(project);
        taskService.add(task);

        Assert.assertThrows(AuthRequiredException.class, () -> service.unbindTaskFromProject(NULL_USER_ID, project.getId(), task.getId()));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.unbindTaskFromProject(NON_EXISTENT_USER_ID, project.getId(), task.getId()));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.unbindTaskFromProject(user.getId(), NULL_PROJECT_ID, task.getId()));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.unbindTaskFromProject(user.getId(), NON_EXISTENT_PROJECT_ID, task.getId()));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.unbindTaskFromProject(user.getId(), project.getId(), NULL_TASK_ID));
        Assert.assertThrows(TaskNotFoundException.class, () -> service.unbindTaskFromProject(user.getId(), project.getId(), NON_EXISTENT_TASK_ID));

        @Nullable final Task taskUnbind = service.unbindTaskFromProject(user.getId(), project.getId(), task.getId());
        Assert.assertNotNull(taskUnbind);
        Assert.assertEquals(task.getId(), taskUnbind.getId());
        Assert.assertEquals(NULL_PROJECT_ID, taskUnbind.getProjectId());

        task.setProjectId(project.getId());
    }

}

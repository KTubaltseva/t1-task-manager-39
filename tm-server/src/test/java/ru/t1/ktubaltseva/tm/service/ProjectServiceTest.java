package ru.t1.ktubaltseva.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.ktubaltseva.tm.api.service.*;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.ProjectNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.*;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.User;

import java.util.Collection;
import java.util.Collections;

import static ru.t1.ktubaltseva.tm.constant.ProjectTestData.*;

@Category(UnitCategory.class)
public final class ProjectServiceTest {

    @NotNull
    private static final ILoggerService loggerService = new LoggerService();

    @NotNull
    private static final IPropertyService propertyService = new PropertyService(loggerService);

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectService service = new ProjectService(connectionService);

    @NotNull
    private static final IUserService userService = new UserService(propertyService, connectionService);

    @BeforeClass
    @SneakyThrows
    public static void before() {
        userService.add(USER_1);
        userService.add(USER_2);
    }

    @AfterClass
    @SneakyThrows
    public static void afterClazz() {
        userService.removeOne(USER_1);
        userService.removeOne(USER_2);
    }

    @After
    @SneakyThrows
    public void after() {
        service.clear(USER_1.getId());
        service.clear(USER_2.getId());
    }

    @Test
    public void add() throws AbstractException {
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.add(NULL_PROJECT));

        @Nullable final Project projectToAdd = USER_1_PROJECT_1;
        @Nullable final String projectToAddId = projectToAdd.getId();

        @Nullable final Project projectAdded = service.add(projectToAdd);
        Assert.assertNotNull(projectAdded);
        Assert.assertEquals(projectToAdd.getId(), projectAdded.getId());

        @Nullable final Project projectFindOneById = service.findOneById(projectToAddId);
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(projectToAdd.getId(), projectFindOneById.getId());
    }

    @Test
    public void addByUserId() throws AbstractException {
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.add(USER_1.getId(), NULL_PROJECT));
        Assert.assertThrows(AuthRequiredException.class, () -> service.add(NULL_USER_ID, USER_1_PROJECT_1));

        @Nullable final String userToAddId = USER_1.getId();
        @Nullable final String userNoAddId = USER_2.getId();
        @Nullable final Project projectToAddByUser = USER_1_PROJECT_1;
        @Nullable final String projectToAddByUserId = projectToAddByUser.getId();

        @Nullable final Project projectAddedByUser = service.add(userToAddId, projectToAddByUser);
        Assert.assertNotNull(projectAddedByUser);
        Assert.assertTrue(service.existsById(projectToAddByUserId));

        @Nullable final Project projectFindOneById = service.findOneById(projectToAddByUserId);
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(projectAddedByUser.getId(), projectFindOneById.getId());

        @Nullable final Project projectFindOneByIdByUserIdToAdd = service.findOneById(userToAddId, projectToAddByUserId);
        Assert.assertNotNull(projectFindOneByIdByUserIdToAdd);
        Assert.assertEquals(projectAddedByUser.getId(), projectFindOneByIdByUserIdToAdd.getId());

        Assert.assertThrows(ProjectNotFoundException.class, () -> service.findOneById(userNoAddId, projectToAddByUserId));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.findOneById(userToAddId, NON_EXISTENT_PROJECT_ID));
    }

    @Test
    public void addMany() throws AbstractException {
        @Nullable final Collection<Project> projectList = service.add(PROJECT_LIST);
        Assert.assertNotNull(projectList);
        for (@NotNull final Project project : PROJECT_LIST) {
            @Nullable final Project projectFindOneById = service.findOneById(project.getId());
            Assert.assertEquals(project.getId(), projectFindOneById.getId());
        }
    }

    @Test
    public void findOneById() throws AbstractException {
        @NotNull final Project projectExists = USER_1_PROJECT_1;
        service.add(projectExists);

        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(NULL_PROJECT_ID));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.findOneById(NON_EXISTENT_PROJECT_ID));

        @Nullable final Project projectFindOneById = service.findOneById(projectExists.getId());
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(projectExists.getId(), projectFindOneById.getId());
    }

    @Test
    public void findOneByIdByUserId() throws AbstractException {
        @NotNull final Project projectExists = USER_1_PROJECT_1;
        @NotNull final User userExists = USER_1;
        service.add(userExists.getId(), projectExists);

        Assert.assertThrows(AuthRequiredException.class, () -> service.findOneById(NULL_USER_ID, projectExists.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(userExists.getId(), NULL_PROJECT_ID));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.findOneById(userExists.getId(), NON_EXISTENT_PROJECT_ID));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.findOneById(NON_EXISTENT_USER_ID, projectExists.getId()));

        @Nullable final Project projectFindOneById = service.findOneById(userExists.getId(), projectExists.getId());
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(projectExists.getId(), projectFindOneById.getId());
    }

    @Test
    public void findAll() throws AbstractException {
        @NotNull final Project projectExists = USER_1_PROJECT_1;

        service.add(projectExists);
        @NotNull final Collection<Project> projectsFindAllNoEmpty = service.findAll();
        Assert.assertNotNull(projectsFindAllNoEmpty);
    }

    @Test
    public void findAllByUserId() throws AbstractException {
        Assert.assertThrows(AuthRequiredException.class, () -> service.findAll(NULL_USER_ID));

        @NotNull final Project projectExists = USER_1_PROJECT_1;
        @NotNull final User userExists = USER_1;
        service.add(userExists.getId(), projectExists);

        @NotNull final Collection<Project> projectsFindAllByUserRepNoEmpty = service.findAll(userExists.getId());
        Assert.assertNotNull(projectsFindAllByUserRepNoEmpty);

        @NotNull final Collection<Project> projectsFindAllByNonExistentUser = service.findAll(NON_EXISTENT_USER_ID);
        Assert.assertNotNull(projectsFindAllByNonExistentUser);
        Assert.assertEquals(Collections.emptyList(), projectsFindAllByNonExistentUser);
    }

    @Test
    public void clearByUserId() throws AbstractException {
        Assert.assertThrows(AuthRequiredException.class, () -> service.clear(NULL_USER_ID));

        @NotNull final User userToClear = USER_1;
        @NotNull final User userNoClear = USER_2;
        @NotNull final String userToClearId = userToClear.getId();
        @NotNull final String userNoClearId = userNoClear.getId();
        @NotNull final Collection<Project> projectByUserToClearList = service.add(USER_1_PROJECT_LIST);
        @NotNull final Collection<Project> projectByUserNoClearList = service.add(USER_2_PROJECT_LIST);
        service.clear(userToClearId);

        for (@NotNull final Project projectByUserToClear : projectByUserToClearList) {
            Assert.assertThrows(ProjectNotFoundException.class, () -> service.findOneById(projectByUserToClear.getId()));
        }
        Assert.assertEquals(0, service.findAll(userToClearId).size());

        for (@NotNull final Project projectByUserNoClear : projectByUserNoClearList) {
            @Nullable final Project projectFindOneById = service.findOneById(projectByUserNoClear.getId());
            Assert.assertEquals(projectByUserNoClear.getId(), projectFindOneById.getId());
        }
        Assert.assertNotEquals(0, service.findAll(userNoClearId).size());
    }

    @Test
    public void removeOne() throws AbstractException {
        @Nullable final Project projectToRemove = USER_1_PROJECT_1;
        service.add((projectToRemove));

        Assert.assertThrows(ProjectNotFoundException.class, () -> service.removeOne(NULL_PROJECT));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.removeOne(NON_EXISTENT_PROJECT));

        service.removeOne(projectToRemove);

        Assert.assertThrows(ProjectNotFoundException.class, () -> service.findOneById(projectToRemove.getId()));
    }

    @Test
    public void removeOneByUserId() throws AbstractException {
        @NotNull final User userToRemove = USER_1;
        @NotNull final String userToRemoveId = userToRemove.getId();
        @Nullable final Project projectByUserToRemove = service.add((USER_1_PROJECT_1));
        @Nullable final Project projectByUserNoRemove = service.add((USER_2_PROJECT_1));

        Assert.assertThrows(ProjectNotFoundException.class, () -> service.removeOne(NON_EXISTENT_USER_ID, projectByUserToRemove));
        Assert.assertThrows(AuthRequiredException.class, () -> service.removeOne(NULL_USER_ID, projectByUserToRemove));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.removeOne(userToRemoveId, NULL_PROJECT));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.removeOne(userToRemoveId, NON_EXISTENT_PROJECT));

        service.removeOne(userToRemoveId, projectByUserToRemove);

        Assert.assertThrows(ProjectNotFoundException.class, () -> service.findOneById(projectByUserToRemove.getId()));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.removeOne(userToRemoveId, projectByUserNoRemove));

        @Nullable final Project projectNoRemovedFindOneById = service.findOneById(projectByUserNoRemove.getId());
        Assert.assertNotNull(projectNoRemovedFindOneById);
        Assert.assertEquals(projectNoRemovedFindOneById.getId(), projectByUserNoRemove.getId());
    }

    @Test
    public void removeById() throws AbstractException {
        @Nullable final Project projectToRemove = USER_1_PROJECT_1;
        service.add((projectToRemove));

        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(NULL_PROJECT_ID));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.removeById(NON_EXISTENT_PROJECT_ID));

        service.removeById(projectToRemove.getId());

        Assert.assertThrows(ProjectNotFoundException.class, () -> service.findOneById(projectToRemove.getId()));
    }

    @Test
    public void removeByIdByUserId() throws AbstractException {
        @NotNull final User userToRemove = USER_1;
        @NotNull final String userToRemoveId = userToRemove.getId();
        @Nullable final Project projectByUserToRemove = service.add((USER_1_PROJECT_1));
        @Nullable final Project projectByUserNoRemove = service.add((USER_2_PROJECT_1));

        Assert.assertThrows(ProjectNotFoundException.class, () -> service.removeById(NON_EXISTENT_USER_ID, projectByUserToRemove.getId()));
        Assert.assertThrows(AuthRequiredException.class, () -> service.removeById(NULL_USER_ID, projectByUserToRemove.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(userToRemoveId, NULL_PROJECT_ID));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.removeById(userToRemoveId, NON_EXISTENT_PROJECT_ID));

        service.removeById(userToRemoveId, projectByUserToRemove.getId());

        Assert.assertThrows(ProjectNotFoundException.class, () -> service.findOneById(projectByUserToRemove.getId()));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.removeById(userToRemoveId, projectByUserNoRemove.getId()));

        @Nullable final Project projectNoRemovedFindOneById = service.findOneById(projectByUserNoRemove.getId());
        Assert.assertNotNull(projectNoRemovedFindOneById);
        Assert.assertEquals(projectNoRemovedFindOneById.getId(), projectByUserNoRemove.getId());
    }

    @Test
    public void isExists() throws AbstractException {
        @NotNull final Project projectExists = USER_1_PROJECT_1;
        service.add(projectExists);

        Assert.assertThrows(IdEmptyException.class, () -> service.existsById(NULL_PROJECT_ID));

        Assert.assertFalse(service.existsById(NON_EXISTENT_PROJECT_ID));
        Assert.assertTrue(service.existsById(projectExists.getId()));
    }

    @Test
    public void createName() throws AbstractException {
        @NotNull final User existentUser = USER_1;
        Assert.assertThrows(AuthRequiredException.class, () -> service.create(NULL_USER_ID, PROJECT_NAME));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(existentUser.getId(), NULL_NAME));

        @NotNull final Project createdProject = service.create(existentUser.getId(), PROJECT_NAME);
        Assert.assertNotNull(createdProject);
        Assert.assertEquals(PROJECT_NAME, createdProject.getName());
        Assert.assertTrue(service.existsById(createdProject.getId()));

        @Nullable final Project projectFindOneById = service.findOneById(createdProject.getId());
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(createdProject.getId(), projectFindOneById.getId());
    }

    @Test
    public void createNameDesc() throws AbstractException {
        @NotNull final User existentUser = USER_1;
        Assert.assertThrows(AuthRequiredException.class, () -> service.create(NULL_USER_ID, PROJECT_NAME, PROJECT_DESC));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(existentUser.getId(), NULL_NAME, PROJECT_DESC));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.create(existentUser.getId(), PROJECT_NAME, NULL_DESC));

        @NotNull final Project createdProject = service.create(existentUser.getId(), PROJECT_NAME, PROJECT_DESC);
        Assert.assertNotNull(createdProject);
        Assert.assertEquals(PROJECT_NAME, createdProject.getName());
        Assert.assertEquals(PROJECT_DESC, createdProject.getDescription());
        Assert.assertTrue(service.existsById(createdProject.getId()));

        @Nullable final Project projectFindOneById = service.findOneById(createdProject.getId());
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(createdProject.getId(), projectFindOneById.getId());
    }

    @Test
    public void changeProjectStatusById() throws AbstractException {
        @NotNull final User userToUpdate = USER_1;
        @NotNull final User userNoUpdate = USER_2;
        @Nullable final Project projectToUpdate = service.add((USER_1_PROJECT_1));
        @Nullable final Project projectNoUpdate = service.add((USER_1_PROJECT_2));

        Assert.assertThrows(AuthRequiredException.class, () -> service.changeProjectStatusById(NULL_USER_ID, projectToUpdate.getId(), PROJECT_STATUS));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.changeProjectStatusById(NON_EXISTENT_USER_ID, projectToUpdate.getId(), PROJECT_STATUS));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.changeProjectStatusById(userNoUpdate.getId(), projectToUpdate.getId(), PROJECT_STATUS));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.changeProjectStatusById(userToUpdate.getId(), NULL_PROJECT_ID, PROJECT_STATUS));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.changeProjectStatusById(userToUpdate.getId(), NON_EXISTENT_PROJECT_ID, PROJECT_STATUS));
        Assert.assertThrows(StatusEmptyException.class, () -> service.changeProjectStatusById(userToUpdate.getId(), projectToUpdate.getId(), NULL_STATUS));

        @Nullable final Project projectUpdated = service.changeProjectStatusById(userToUpdate.getId(), projectToUpdate.getId(), PROJECT_STATUS);
        Assert.assertNotNull(projectUpdated);
        Assert.assertEquals(projectUpdated.getId(), projectToUpdate.getId());
        Assert.assertEquals(PROJECT_STATUS, projectUpdated.getStatus());

        @Nullable final Project projectFindOneByIdToUpdate = service.findOneById(projectToUpdate.getId());
        Assert.assertNotNull(projectFindOneByIdToUpdate);
        Assert.assertEquals(projectFindOneByIdToUpdate.getId(), projectToUpdate.getId());
        Assert.assertEquals(PROJECT_STATUS, projectFindOneByIdToUpdate.getStatus());

        @Nullable final Project projectFindOneByIdNoUpdate = service.findOneById(projectNoUpdate.getId());
        Assert.assertNotNull(projectFindOneByIdNoUpdate);
        Assert.assertEquals(projectFindOneByIdNoUpdate.getId(), projectNoUpdate.getId());
        Assert.assertNotEquals(PROJECT_STATUS, projectFindOneByIdNoUpdate.getStatus());
    }

    @Test
    public void updateProjectById() throws AbstractException {
        @NotNull final User userToUpdate = USER_1;
        @NotNull final User userNoUpdate = USER_2;
        @Nullable final Project projectToUpdate = service.add((USER_1_PROJECT_1));
        @Nullable final Project projectNoUpdate = service.add((USER_1_PROJECT_2));

        Assert.assertThrows(AuthRequiredException.class, () -> service.updateById(NULL_USER_ID, projectToUpdate.getId(), PROJECT_NAME, PROJECT_DESC));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.updateById(NON_EXISTENT_USER_ID, projectToUpdate.getId(), PROJECT_NAME, PROJECT_DESC));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.updateById(userNoUpdate.getId(), projectToUpdate.getId(), PROJECT_NAME, PROJECT_DESC));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.updateById(userToUpdate.getId(), NULL_PROJECT_ID, PROJECT_NAME, PROJECT_DESC));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.updateById(userToUpdate.getId(), NON_EXISTENT_PROJECT_ID, PROJECT_NAME, PROJECT_DESC));
        Assert.assertThrows(NameEmptyException.class, () -> service.updateById(userToUpdate.getId(), projectToUpdate.getId(), NULL_NAME, PROJECT_DESC));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.updateById(userToUpdate.getId(), projectToUpdate.getId(), PROJECT_NAME, NULL_DESC));

        @Nullable final Project projectUpdated = service.updateById(userToUpdate.getId(), projectToUpdate.getId(), PROJECT_NAME, PROJECT_DESC);
        Assert.assertNotNull(projectUpdated);
        Assert.assertEquals(projectUpdated.getId(), projectToUpdate.getId());
        Assert.assertEquals(PROJECT_NAME, projectUpdated.getName());
        Assert.assertEquals(PROJECT_DESC, projectUpdated.getDescription());

        @Nullable final Project projectFindOneByIdToUpdate = service.findOneById(projectToUpdate.getId());
        Assert.assertNotNull(projectFindOneByIdToUpdate);
        Assert.assertEquals(projectFindOneByIdToUpdate.getId(), projectToUpdate.getId());
        Assert.assertEquals(PROJECT_NAME, projectFindOneByIdToUpdate.getName());
        Assert.assertEquals(PROJECT_DESC, projectFindOneByIdToUpdate.getDescription());

        @Nullable final Project projectFindOneByIdNoUpdate = service.findOneById(projectNoUpdate.getId());
        Assert.assertNotNull(projectFindOneByIdNoUpdate);
        Assert.assertEquals(projectFindOneByIdNoUpdate.getId(), projectNoUpdate.getId());
        Assert.assertNotEquals(PROJECT_NAME, projectFindOneByIdNoUpdate.getName());
        Assert.assertNotEquals(PROJECT_DESC, projectFindOneByIdNoUpdate.getDescription());
    }

}

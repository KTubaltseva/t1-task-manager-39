package ru.t1.ktubaltseva.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.ktubaltseva.tm.api.service.*;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;
import ru.t1.ktubaltseva.tm.model.Session;
import ru.t1.ktubaltseva.tm.model.User;

import java.util.Collection;
import java.util.Collections;

import static ru.t1.ktubaltseva.tm.constant.SessionTestData.*;

@Category(UnitCategory.class)
public final class SessionServiceTest {

    @NotNull
    private static final ILoggerService loggerService = new LoggerService();

    @NotNull
    private static final IPropertyService propertyService = new PropertyService(loggerService);

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ISessionService service = new SessionService(connectionService);

    @NotNull
    private static final IUserService userService = new UserService(propertyService, connectionService);

    @BeforeClass
    @SneakyThrows
    public static void before() {
        userService.add(USER_1);
        userService.add(USER_2);
    }

    @AfterClass
    @SneakyThrows
    public static void afterClazz() {
        userService.removeOne(USER_1);
        userService.removeOne(USER_2);
    }

    @After
    @SneakyThrows
    public void after() {
        service.clear(USER_1.getId());
        service.clear(USER_2.getId());
    }

    @Test
    public void add() throws AbstractException {
        Assert.assertThrows(EntityNotFoundException.class, () -> service.add(NULL_SESSION));

        @Nullable final Session sessionToAdd = USER_1_SESSION_1;
        @Nullable final String sessionToAddId = sessionToAdd.getId();

        @Nullable final Session sessionAdded = service.add(sessionToAdd);
        Assert.assertNotNull(sessionAdded);
        Assert.assertEquals(sessionToAdd.getId(), sessionAdded.getId());

        @Nullable final Session sessionFindOneById = service.findOneById(sessionToAddId);
        Assert.assertNotNull(sessionFindOneById);
        Assert.assertEquals(sessionToAdd.getId(), sessionFindOneById.getId());
    }

    @Test
    public void addByUserId() throws AbstractException {
        Assert.assertThrows(EntityNotFoundException.class, () -> service.add(USER_1.getId(), NULL_SESSION));
        Assert.assertThrows(AuthRequiredException.class, () -> service.add(NULL_USER_ID, USER_1_SESSION_1));

        @Nullable final String userToAddId = USER_1.getId();
        @Nullable final String userNoAddId = USER_2.getId();
        @Nullable final Session sessionToAddByUser = USER_1_SESSION_1;
        @Nullable final String sessionToAddByUserId = sessionToAddByUser.getId();

        @Nullable final Session sessionAddedByUser = service.add(userToAddId, sessionToAddByUser);
        Assert.assertNotNull(sessionAddedByUser);
        Assert.assertTrue(service.existsById(sessionToAddByUserId));

        @Nullable final Session sessionFindOneById = service.findOneById(sessionToAddByUserId);
        Assert.assertNotNull(sessionFindOneById);
        Assert.assertEquals(sessionAddedByUser.getId(), sessionFindOneById.getId());

        @Nullable final Session sessionFindOneByIdByUserIdToAdd = service.findOneById(userToAddId, sessionToAddByUserId);
        Assert.assertNotNull(sessionFindOneByIdByUserIdToAdd);
        Assert.assertEquals(sessionAddedByUser.getId(), sessionFindOneByIdByUserIdToAdd.getId());

        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(userNoAddId, sessionToAddByUserId));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(userToAddId, NON_EXISTENT_SESSION_ID));
    }

    @Test
    public void addMany() throws AbstractException {
        @Nullable final Collection<Session> sessionList = service.add(SESSION_LIST);
        Assert.assertNotNull(sessionList);
        for (@NotNull final Session session : SESSION_LIST) {
            @Nullable final Session sessionFindOneById = service.findOneById(session.getId());
            Assert.assertEquals(session.getId(), sessionFindOneById.getId());
        }
    }

    @Test
    public void findOneById() throws AbstractException {
        @NotNull final Session sessionExists = USER_1_SESSION_1;
        service.add(sessionExists);

        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(NULL_SESSION_ID));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(NON_EXISTENT_SESSION_ID));

        @Nullable final Session sessionFindOneById = service.findOneById(sessionExists.getId());
        Assert.assertNotNull(sessionFindOneById);
        Assert.assertEquals(sessionExists.getId(), sessionFindOneById.getId());
    }

    @Test
    public void findOneByIdByUserId() throws AbstractException {
        @NotNull final Session sessionExists = USER_1_SESSION_1;
        @NotNull final User userExists = USER_1;
        service.add(userExists.getId(), sessionExists);

        Assert.assertThrows(AuthRequiredException.class, () -> service.findOneById(NULL_USER_ID, sessionExists.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(userExists.getId(), NULL_SESSION_ID));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(userExists.getId(), NON_EXISTENT_SESSION_ID));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(NON_EXISTENT_USER_ID, sessionExists.getId()));

        @Nullable final Session sessionFindOneById = service.findOneById(userExists.getId(), sessionExists.getId());
        Assert.assertNotNull(sessionFindOneById);
        Assert.assertEquals(sessionExists.getId(), sessionFindOneById.getId());
    }

    @Test
    public void findAll() throws AbstractException {
        @NotNull final Session sessionExists = USER_1_SESSION_1;

        service.add(sessionExists);
        @NotNull final Collection<Session> sessionsFindAllNoEmpty = service.findAll();
        Assert.assertNotNull(sessionsFindAllNoEmpty);
    }

    @Test
    public void findAllByUserId() throws AbstractException {
        Assert.assertThrows(AuthRequiredException.class, () -> service.findAll(NULL_USER_ID));

        @NotNull final Session sessionExists = USER_1_SESSION_1;
        @NotNull final User userExists = USER_1;

        service.add(userExists.getId(), sessionExists);
        @NotNull final Collection<Session> sessionsFindAllByUserRepNoEmpty = service.findAll(userExists.getId());
        Assert.assertNotNull(sessionsFindAllByUserRepNoEmpty);

        @NotNull final Collection<Session> sessionsFindAllByNonExistentUser = service.findAll(NON_EXISTENT_USER_ID);
        Assert.assertNotNull(sessionsFindAllByNonExistentUser);
        Assert.assertEquals(Collections.emptyList(), sessionsFindAllByNonExistentUser);
    }

    @Test
    public void clearByUserId() throws AbstractException {
        Assert.assertThrows(AuthRequiredException.class, () -> service.clear(NULL_USER_ID));

        @NotNull final User userToClear = USER_1;
        @NotNull final User userNoClear = USER_2;
        @NotNull final String userToClearId = userToClear.getId();
        @NotNull final String userNoClearId = userNoClear.getId();
        @NotNull final Collection<Session> sessionByUserToClearList = service.add(USER_1_SESSION_LIST);
        @NotNull final Collection<Session> sessionByUserNoClearList = service.add(USER_2_SESSION_LIST);
        service.clear(userToClearId);

        for (@NotNull final Session sessionByUserToClear : sessionByUserToClearList) {
            Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(sessionByUserToClear.getId()));
        }
        Assert.assertEquals(0, service.findAll(userToClearId).size());

        for (@NotNull final Session sessionByUserNoClear : sessionByUserNoClearList) {
            @Nullable final Session sessionFindOneById = service.findOneById(sessionByUserNoClear.getId());
            Assert.assertEquals(sessionByUserNoClear.getId(), sessionFindOneById.getId());
        }
        Assert.assertNotEquals(0, service.findAll(userNoClearId).size());
    }

    @Test
    public void removeOne() throws AbstractException {
        @Nullable final Session sessionToRemove = USER_1_SESSION_1;
        service.add((sessionToRemove));

        Assert.assertThrows(EntityNotFoundException.class, () -> service.removeOne(NULL_SESSION));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.removeOne(NON_EXISTENT_SESSION));

        service.removeOne(sessionToRemove);

        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(sessionToRemove.getId()));
    }

    @Test
    public void removeOneByUserId() throws AbstractException {
        @NotNull final User userToRemove = USER_1;
        @NotNull final String userToRemoveId = userToRemove.getId();
        @Nullable final Session sessionByUserToRemove = service.add((USER_1_SESSION_1));
        @Nullable final Session sessionByUserNoRemove = service.add((USER_2_SESSION_1));

        Assert.assertThrows(EntityNotFoundException.class, () -> service.removeOne(NON_EXISTENT_USER_ID, sessionByUserToRemove));
        Assert.assertThrows(AuthRequiredException.class, () -> service.removeOne(NULL_USER_ID, sessionByUserToRemove));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.removeOne(userToRemoveId, NULL_SESSION));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.removeOne(userToRemoveId, NON_EXISTENT_SESSION));

        service.removeOne(userToRemoveId, sessionByUserToRemove);

        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(sessionByUserToRemove.getId()));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.removeOne(userToRemoveId, sessionByUserNoRemove));

        @Nullable final Session sessionNoRemovedFindOneById = service.findOneById(sessionByUserNoRemove.getId());
        Assert.assertNotNull(sessionNoRemovedFindOneById);
        Assert.assertEquals(sessionNoRemovedFindOneById.getId(), sessionByUserNoRemove.getId());
    }

    @Test
    public void removeById() throws AbstractException {
        @Nullable final Session sessionToRemove = USER_1_SESSION_1;
        service.add((sessionToRemove));

        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(NULL_SESSION_ID));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.removeById(NON_EXISTENT_SESSION_ID));

        service.removeById(sessionToRemove.getId());

        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(sessionToRemove.getId()));
    }

    @Test
    public void removeByIdByUserId() throws AbstractException {
        @NotNull final User userToRemove = USER_1;
        @NotNull final String userToRemoveId = userToRemove.getId();
        @Nullable final Session sessionByUserToRemove = service.add((USER_1_SESSION_1));
        @Nullable final Session sessionByUserNoRemove = service.add((USER_2_SESSION_1));

        Assert.assertThrows(EntityNotFoundException.class, () -> service.removeById(NON_EXISTENT_USER_ID, sessionByUserToRemove.getId()));
        Assert.assertThrows(AuthRequiredException.class, () -> service.removeById(NULL_USER_ID, sessionByUserToRemove.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(userToRemoveId, NULL_SESSION_ID));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.removeById(userToRemoveId, NON_EXISTENT_SESSION_ID));

        service.removeById(userToRemoveId, sessionByUserToRemove.getId());

        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(sessionByUserToRemove.getId()));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.removeById(userToRemoveId, sessionByUserNoRemove.getId()));

        @Nullable final Session sessionNoRemovedFindOneById = service.findOneById(sessionByUserNoRemove.getId());
        Assert.assertNotNull(sessionNoRemovedFindOneById);
        Assert.assertEquals(sessionNoRemovedFindOneById.getId(), sessionByUserNoRemove.getId());
    }

    @Test
    public void isExists() throws AbstractException {
        @NotNull final Session sessionExists = USER_1_SESSION_1;
        service.add(sessionExists);

        Assert.assertThrows(IdEmptyException.class, () -> service.existsById(NULL_SESSION_ID));

        Assert.assertFalse(service.existsById(NON_EXISTENT_SESSION_ID));
        Assert.assertTrue(service.existsById(sessionExists.getId()));
    }

}

package ru.t1.ktubaltseva.tm.dto.request.task;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TaskCreateRequest extends AbstractUserRequest {

    @Nullable
    private String name;

    @Nullable
    private String description;

    public TaskCreateRequest(@Nullable final String token) {
        super(token);
    }

    public TaskCreateRequest(@Nullable final String token, @Nullable final String name, @Nullable final String description) {
        super(token);
        this.name = name;
        this.description = description;
    }

}

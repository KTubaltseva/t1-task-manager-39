package ru.t1.ktubaltseva.tm.dto.response.task;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.response.AbstractResponse;
import ru.t1.ktubaltseva.tm.model.Task;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractTaskResponse extends AbstractResponse {

    @Nullable
    private Task task;

}

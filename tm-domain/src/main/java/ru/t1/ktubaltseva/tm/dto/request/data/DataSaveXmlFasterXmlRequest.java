package ru.t1.ktubaltseva.tm.dto.request.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.request.AbstractUserRequest;

@NoArgsConstructor
public class DataSaveXmlFasterXmlRequest extends AbstractUserRequest {

    public DataSaveXmlFasterXmlRequest(@Nullable final String token) {
        super(token);
    }

}

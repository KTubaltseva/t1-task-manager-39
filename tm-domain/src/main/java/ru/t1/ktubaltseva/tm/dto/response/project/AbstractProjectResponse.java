package ru.t1.ktubaltseva.tm.dto.response.project;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.response.AbstractResponse;
import ru.t1.ktubaltseva.tm.model.Project;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractProjectResponse extends AbstractResponse {

    @Nullable
    private Project project;

}

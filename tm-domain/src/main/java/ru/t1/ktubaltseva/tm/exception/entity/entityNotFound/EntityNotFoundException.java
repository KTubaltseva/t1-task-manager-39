package ru.t1.ktubaltseva.tm.exception.entity.entityNotFound;

import org.jetbrains.annotations.NotNull;

public final class EntityNotFoundException extends AbstractEntityNotFoundException {

    public EntityNotFoundException() {
        super("Error! Entity not found...");
    }

    public EntityNotFoundException(@NotNull final String message) {
        super("Error! Entity " + message + " not found...");
    }
}

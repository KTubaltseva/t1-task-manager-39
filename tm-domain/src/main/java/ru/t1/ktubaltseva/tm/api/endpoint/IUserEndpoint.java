package ru.t1.ktubaltseva.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.dto.request.user.*;
import ru.t1.ktubaltseva.tm.dto.response.user.*;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.security.NoSuchAlgorithmException;

import static ru.t1.ktubaltseva.tm.api.endpoint.IEndpoint.*;

@WebService
public interface IUserEndpoint {

    @NotNull
    String NAME = "UserEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @WebMethod(exclude = true)
    static IUserEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @WebMethod(exclude = true)
    static IUserEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, IUserEndpoint.class);
    }

    @WebMethod(exclude = true)
    static IUserEndpoint newInstance(
            @NotNull final String host,
            @NotNull final String port
    ) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IUserEndpoint.class);
    }

    @NotNull
    @WebMethod
    UserChangePasswordResponse changePassword(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserChangePasswordRequest request
    ) throws NoSuchAlgorithmException, AbstractException;

    @NotNull
    @WebMethod
    UserLockResponse lock(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLockRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    UserRegistryResponse registry(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserRegistryRequest request
    ) throws AbstractException, NoSuchAlgorithmException;

    @NotNull
    @WebMethod
    UserRemoveResponse remove(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserRemoveRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    UserUnlockResponse unlock(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserUnlockRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    UserUpdateProfileResponse updateProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserUpdateProfileRequest request
    ) throws AbstractException;

}

package ru.t1.ktubaltseva.tm.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.model.IWBS;
import ru.t1.ktubaltseva.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Task extends AbstractUserOwnedModel implements IWBS {

    private static final long serialVersionUID = 1;

    @NotNull
    private String name = "";

    @Nullable
    private String description;

    @NotNull
    private Status status = Status.NOT_STARTED;

    @Nullable
    private String projectId;

    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm a z")
    private Date created = new Date();

    public Task(@NotNull final String name) {
        this.name = name;
    }

    public Task(
            @NotNull final String name,
            @NotNull final String description
    ) {
        this.name = name;
        this.description = description;
    }

    public Task(
            @NotNull final String name,
            @NotNull final String description,
            @NotNull final Status status
    ) {
        this.name = name;
        this.description = description;
        this.status = status;
    }

    public Task(@NotNull final User user, @NotNull final String name) {
        setUserId(user.getId());
        this.name = name;
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull String result = "";
        result += name;
        if (description != null)
            result += "\t(" + description + ")";
        result += "\t" + Status.toName(status) + "";
        return result;
    }

}

package ru.t1.ktubaltseva.tm.dto.response.data;

import lombok.NoArgsConstructor;
import ru.t1.ktubaltseva.tm.dto.response.AbstractResponse;

@NoArgsConstructor
public class DataSaveJsonJaxBResponse extends AbstractResponse {

}
